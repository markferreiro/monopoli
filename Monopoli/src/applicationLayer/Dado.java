/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : Dado.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que modela un dado de 6 caras.
 *
 ******************************************************************************/

package applicationLayer;

import java.io.Serializable;

public class Dado implements Serializable
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE CLASE                                                  |
    +------------------------------------------------------------------------*/
    private byte valorDado;

    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     *
     */
    public Dado()
    {
        /* El dado aun no ha sido usado */
        this.valorDado = 0;
    }

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICOS                                           |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     * 
     * @return el valor obtenido en la tirada del dado.
     */
    public int tirar()
    {
        this.valorDado =  ( byte )( Math.floor( ( Math.random() * 6 ) + 1 ) );
        return( int )( this.valorDado );
    }

    /**-------------------------------------------------------------------------
     * 
     * @return el último valor del dado
     */
    public byte obtenerUltimoValor()
    {
        return valorDado;
    }
}
