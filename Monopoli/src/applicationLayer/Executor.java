package applicationLayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;

import applicationLayer.casilla.Casilla;
import applicationLayer.casilla.ICasilla;
import presentationLayer.ErrorExcepcion;
import presentationLayer.JTablero;
import presentationLayer.Utils;

public class Executor {
	public static JTablero taulell;
	public static Juego game;
	private Runnable runner;
	public static Boolean juegoIniciado;
	public static Boolean haTiradoJugadorElDado = false;

	public Executor() {
		// TODO Auto-generated constructor stub
	}
	public Executor(Juego t) {
		game = t;
		Executor.taulell = new JTablero();
		Executor.taulell.repintarTablero();
		Executor.taulell.setVisible(true);
		Executor.juegoIniciado = false;
	}
	public static void iniciaElJuego() throws InterruptedException {
		Jugador gamer;
		while ( !Executor.getGame().estaJuegoFinalizado() ) {
			gamer = game.obtenerJugadorConTurno();
			if (gamer.esPC()) {
				Executor.juegaPC(gamer);
			} else {
				if (!Executor.haTiradoJugadorElDado) {
					Executor.printMessage("Esperando a que jugador " + gamer.obtenerNombre() + " tire el dado.");

					do {
						Thread.yield();
					} while (!JTablero.isReady);

					JTablero.btnStartGame.setText("Tirar dado");
					JTablero.btnStartGame.setVisible(true);
					return;
				} else {
					Executor.juegaJugador(gamer);
					Executor.haTiradoJugadorElDado = false;
				}
			}
		}
	}
	public static void juegaPC(Jugador gamer) throws InterruptedException {
		int dado, casillaActual, precioCasillaActual, precioPorCaerCasillaActual, dineroDisponibleJugadorConTurno;
		Jugador jugadorPropietarioDeCasilla;

		Executor.printMessage("Jugador " + gamer.obtenerNombre() + " tirar� el dado...");

		do {
			Thread.yield();
		} while (!JTablero.isReady);

		dado = Executor.getGame().tirarDado();
		JTablero.ejecutarDados(3000, dado);

		do {
			Thread.yield();
		} while (!JTablero.isReady);

		Executor.printMessage("Jugador " + gamer.obtenerNombre() + " movido a la nueva casilla.");

		do {
			Thread.yield();
		} while (!JTablero.isReady);

		

		Executor.primerPaso(Executor.getGame().obtenerPosicionJugadorConTurnoEnTablero(), dado);

		JTablero.perderTiempo("Turno del ordenador terminado. Ha tirado los dados y se ha movido.");
	}
	public static void juegaJugador(Jugador gamer) {
		int dado, casillaActual, precioCasillaActual, precioPorCaerCasillaActual, dineroDisponibleJugadorConTurno;
		Jugador jugadorPropietarioDeCasilla;

		Executor.printMessage("Jugador " + gamer.obtenerNombre() + " tirar� el dado...");

		do {
			Thread.yield();
		} while (!JTablero.isReady);

		dado = Executor.getGame().tirarDado();
		JTablero.ejecutarDados(3000, dado);

		do {
			Thread.yield();
		} while (!JTablero.isReady);

//		Executor.game.moverJugadorConTurnoACasilla(dado);
//		Executor.taulell.repintarTablero();

		do {
			Thread.yield();
		} while (!JTablero.isReady);

		Executor.printMessage("Jugador " + gamer.obtenerNombre() + " movido a la nueva casilla.");

		do {
			Thread.yield();
		} while (!JTablero.isReady);
		
		primerPaso(Executor.getGame().obtenerPosicionJugadorConTurnoEnTablero(), dado);

		JTablero.perderTiempo("Turno del jugador terminado. Has tirado los datos y se te ha movido de casilla.");
	}
	private static Juego getGame() {
		return game;
	}
	private static void printMessage(String txt) {
		JTablero.isReady = false;
		JTablero.lblInstrucciones.setText(txt);
		JTablero.isReady = true;
	}
	
//	Metodo que gestiona la estancia en la carcel del jugador. En caso favorable, lo mueve y llama al metodo que corresponda
//	(Especial o no especial)
	private static void primerPaso(int casillaActual, int dado) {
		int nuevaCasilla;

		if( Executor.getGame().obtenerCategoriaCasilla( casillaActual ).equals( ICasilla.CARCEL ) ) {
			if (dado == 5) {
				Executor.getGame().moverJugadorConTurnoACasilla(dado);
				JTablero.perderTiempo("Enhorabuena! Sacas un 5 y sales de la carcel!");
			} else if (Executor.getGame().obtenerJugadorConTurno().dimeCarcel()) {
				Executor.getGame().moverJugadorConTurnoACasilla(dado);
				JTablero.perderTiempo("Enhorabuena! Ya has estado tus 3 turnos en la carcel, puedes salir!");
			} else {
				JTablero.perderTiempo("Lo siento, un turno mas en la carcel.");
				Executor.getGame().establecerTurnoSiguiente();
			}
			
			Executor.taulell.repintarTablero();
			do {
				Thread.yield();
			} while (!JTablero.isReady);
			
		} else {
			Executor.getGame().moverJugadorConTurnoACasilla(dado);
			nuevaCasilla = Executor.getGame().obtenerPosicionJugadorConTurnoEnTablero();
			
			Executor.taulell.repintarTablero();
			do {
				Thread.yield();
			} while (!JTablero.isReady);
			
			if (Executor.getGame().obtenerTipoCasilla(nuevaCasilla).toLowerCase().contains("especial")) {
				casillaEspecial(nuevaCasilla);
			} else {
				casillaNoEspecial(nuevaCasilla);
			}
		}
	}
	private static void casillaEspecial(int nuevaCasilla) {
		int dado, dado2;
		
		if (Executor.getGame().obtenerCategoriaCasilla(nuevaCasilla).equals( ICasilla.LOTERIA)) {
			if (Executor.getGame().obtenerJugadorConTurno().obtenerDineroDisponible() >= 10) {
				Executor.getGame().pagoLoteria();
				Executor.getGame().sustraerDineroAJugadorConTurno(10);
				JTablero.perderTiempo("Tira el dado dos veces para probar suerte en la loteria!");
				
//				Primer dado
				
				dado = Executor.getGame().tirarDado();
				JTablero.ejecutarDados(3000, dado);

				do {
					Thread.yield();
				} while (!JTablero.isReady);
				
				JTablero.perderTiempo("Primer dado: " + dado + ".");
				
//				Segundo dado
				
				dado2 = Executor.getGame().tirarDado();
				JTablero.ejecutarDados(3000, dado2);

				do {
					Thread.yield();
				} while (!JTablero.isReady);
				
				JTablero.perderTiempo("Segundo dado: " + dado2 + ".");
				
				if (dado != dado2) {
					JTablero.perderTiempo("Que l�stima, no has sacado lo mismo...");
					Executor.getGame().establecerTurnoSiguiente();
				} else {
					JTablero.perderTiempo("Felicidades! Has ganado la loteria!");
					Executor.getGame().obtenerJugadorConTurno().aumentarDineroPorPago(Executor.getGame().ganasLoteria());
					Executor.getGame().establecerTurnoSiguiente();
				}
			}
		} else {
			JTablero.perderTiempo("Has caido en la carcel!");
			Executor.getGame().establecerTurnoSiguiente();
		}
	}
	private static void casillaNoEspecial(int nuevaCasilla) {
		boolean res;
		Juego game = Executor.getGame();
		int posActual = game.obtenerPosicionJugadorConTurnoEnTablero();
		Jugador jugadorActual = game.obtenerJugadorConTurno();
		Jugador jugadorPropietario = game.obtenerJugadorEnPosesionDeCasilla(posActual);
		int precioCompra = Executor.getGame().obtenerPrecioCompraCasilla(posActual);
		int precioPorCaer = Executor.getGame().obtenerPrecioPorCaerEnCasilla(posActual);
		
		if (jugadorPropietario == null) {
			int seleccion = JOptionPane.showOptionDialog(null, "�Quieres comprar la propiedad?", 
					"Seleccione una opci�n", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
					new Object[] { "Si", "No"}, "Si");
			if (seleccion == 0) {
				if (jugadorActual.obtenerDineroDisponible() >= precioCompra) {
					Executor.getGame().establecerCasillaEnPropiedadAJugadorConTurno(posActual);
					Executor.getGame().sustraerDineroAJugadorConTurno(precioCompra);
					JTablero.perderTiempo("Casilla " + Executor.getGame().obtenerCategoriaCasilla(posActual) 
							+ " comprada por jugador " + Executor.getGame().obtenerJugadorConTurno().obtenerNombre());
					game.establecerTurnoSiguiente();
				} else {
					JTablero.perderTiempo("No tienes suficiente dinero para comprar esa casilla.");
					game.establecerTurnoSiguiente();
				}
			}
			res = true;
		} else {
			if (jugadorActual.obtenerDineroDisponible() >= precioPorCaer) {
				Executor.getGame().sustraerDineroAJugadorConTurno(precioPorCaer);
				jugadorPropietario.aumentarDineroPorPago(precioPorCaer);
				JTablero.perderTiempo("Has pagado " + precioPorCaer + " por caer en esta casilla.");
//				AQUI SE LLAMARIA A LA NEGOCIACI�N DEL PROPIETARIO
//				jugadorPropietario.quieresNegociar();
				game.establecerTurnoSiguiente();
				res = false;
			} else {
				game.devolverAlJuegoPosesionesJugadorConTurno();
				game.eliminarJugadorConTurno();
				game.establecerTurnoSiguiente();
				JTablero.perderTiempo("Te has arruinado! ADIOS!");
			}
		}
		
//		return res;
		
	}
}
