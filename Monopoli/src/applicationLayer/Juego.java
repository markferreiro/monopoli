/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : Juego.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que modela un juego del monoply.
 *
 ******************************************************************************/

package applicationLayer;

import java.io.Serializable;
import java.util.ArrayList;

import dataLayer.Persistencia;

public class Juego implements Serializable {
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE CLASE                                                  |
    +------------------------------------------------------------------------*/

    private ArrayList< Jugador > listaJugadores;
    private Tablero              tablero;
    private int                 turnoJugador;
    private Dado                 dado;
    private boolean              juegoFinalizado;
    private int                 codigoErrorProducidoEnPersistencia;
    private int 				contenido;
    
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**-------------------------------------------------------------------------
     *
     * @param listaJugadores: Lista de los jugadores que componen un juego
     * @param tablero       : Tablero con las casillas que componen un juego.
     */
    public Juego( ArrayList< Jugador > listaJugadores, Tablero tablero )
    {
        this.listaJugadores  = listaJugadores;
        this.tablero         = tablero;
        this.turnoJugador    = 0;
        this.dado            = new Dado();
        this.juegoFinalizado = false;
        this.codigoErrorProducidoEnPersistencia = 0;
        this.contenido		 = 0;
    }

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICO[S]                                         |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     *
     * @return la lista de jugadores.
     */
    public ArrayList< Jugador > obtenerListaJugadores()
    {
        return listaJugadores;
    }    
    
    /*
     * 
     * 
     */
    public void pagoLoteria() {
    	this.contenido += 10;
    }
    
    /*
     * 
     * @return dinero que habia en el banco de loteria.
     */
    public int ganasLoteria() {
    	int retorno = this.contenido;
    	
    	this.contenido = 0;
    	
    	return retorno;
    }

    /**-------------------------------------------------------------------------
     *
     * @return el número de casillas que forma un tablero del juego.
     */
    public int obtenerTamanoTablero()
    {
        return this.tablero.obtenerTamanoTablero();
    }

    /**-------------------------------------------------------------------------
     * 
     * @return el tablero actual del juego.
     */
    public Tablero obtenerTablero()
    {
        return this.tablero;
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero : Número de la casilla sobre la cual se quiere
     *                        obtener a el jugador dueño de esta.
     * @return el jugador dueño de la casilla si existe o null en caso contrario.
     */
    public Jugador obtenerJugadorEnPosesionDeCasilla( int casillaNumero )
    {
        return this.tablero.obtenerJugadorEnPosesion( casillaNumero );
    }

    /**-------------------------------------------------------------------------
     *
     * @return el nombre del jugador con turno actual.
     */
    public String nombreJugadorConTurno()
    {
        return this.listaJugadores.get( turnoJugador ).obtenerNombre();
    }
    public Jugador obtenerJugadorConTurno()
    {
        return this.listaJugadores.get( turnoJugador );
    }

    /**-------------------------------------------------------------------------
     *
     * @return el valor obtenido en la tirada del dado.
     */
    public int tirarDado()
    {
        /* Se tira el dado y se devuelve el valor obtenido */
        return( ( int )dado.tirar() );
    }

    /**-------------------------------------------------------------------------
     * 
     * Mueve al jugador con turno actual a la casilla que le corresponda por el
     * despalazamiento dado por "valorMovimiento".
     *
     * @param valorMovimiento : desplazamiento sobre el tablero partiendo de la
     *                          posición actual.
     *
     * @return el ordinal de la casilla a la que ha sido movido.
     */
    public int moverJugadorConTurnoACasilla( int valorMovimiento )
    {
        Jugador auxJugador            = listaJugadores.get( turnoJugador );
        int    posicionActualJugador = auxJugador.obtenerPosicionEnTablero();
        int    siguienteCasilla      = ( ( ( posicionActualJugador + valorMovimiento ) % ( tablero.obtenerTamanoTablero() - 1 ) ) );

        /* Ajustamos la posición si esta es la casilla de SALIDA */
        if( siguienteCasilla == 0 )
        {
            siguienteCasilla++;
        }

        /* Actualizamos la información de la casilla */
        if( auxJugador.obtenerPosicionEnTablero() != 0 )
        {
            this.tablero.quitarJugadorDeCasilla( auxJugador );
        }

        /* Movemos al jugador a la casilla correspondiente */
        auxJugador.establecerPosicionEnTablero( siguienteCasilla );

        /* Actuaizamos la info de la nueva casilla */
        this.tablero.establecerJugadorEnCasilla( auxJugador, siguienteCasilla );

        return siguienteCasilla;
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero : Número de la casilla por la que se quiere hacer
     *                        la consulta.
     *
     * @return la categoría de la casilla número "casillaNumero".
     */
    public String obtenerCategoriaCasilla( int casillaNumero )
    {
        return tablero.obtenerCategoriaCasillaEnPosicion( casillaNumero );
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero : Número de la casilla por la que se quiere hacer
     *                        la consulta.
     *
     * @return el tipo de la casilla número "casillaNumero".
     */
    public String obtenerTipoCasilla(  int casillaNumero )
    {
        return tablero.obtenerTipoCasillaEnPosicion( casillaNumero );
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero : Número de la casilla por la que se quiere hacer
     *                        la consulta.
     *
     * @return true si la casilla tiene dueño o false en caso contrario.
     */
    public boolean estaCasillaAdquirida( int casillaNumero )
    {
        return( tablero.obtenerJugadorEnPosesion( casillaNumero ) != null );
    }

     /**-------------------------------------------------------------------------
     *
     * @param casillaNumero : Número de la casilla por la que se quiere hacer
     *                        la consulta.
     *
     * @return el importe por caer en la casilla "casillaNumero".
     */
    public int obtenerPrecioPorCaerEnCasilla( int casillaNumero )
    {
        return tablero.obtenerCostePorCaerCasillaEnPosicion( casillaNumero );
    }

    /**-------------------------------------------------------------------------
     *
     * @return el dinero disponible que tiene el jugador con turno actual.
     */
    public int obtenerCantidadDineroDisponibleJugadorConTurno()
    {
        return listaJugadores.get( turnoJugador ).obtenerDineroDisponible();
    }

    /**-------------------------------------------------------------------------
     *
     * @param porcentaje : porcentaje que se ha de incrementar el saldo al
     *                     jugador con turno actual.
     */
    public void incrementarDineroEnPorcentajeAJugadorConTurno( int porcentaje )
    {
        int dineroActual = listaJugadores.get( turnoJugador ).obtenerDineroDisponible();
        listaJugadores.get( turnoJugador ).establecerDinero( dineroActual + ( ( dineroActual * porcentaje ) / 100 ) );
    }

    /**-------------------------------------------------------------------------
     *
     * @param cantidadASustraer : cantidad de dinero que se le sutrae al
     *                            jugador con turno actual.
     */
    public void sustraerDineroAJugadorConTurno( int cantidadASustraer )
    {
        int valor = obtenerCantidadDineroDisponibleJugadorConTurno() - cantidadASustraer;

        listaJugadores.get( turnoJugador ).establecerDinero( valor );
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero : Número de la casilla por la que se quiere realizar
     *                        la acción de posesión.
     */
    public void establecerCasillaEnPropiedadAJugadorConTurno( int casillaNumero )
    {
        Jugador auxJugador = listaJugadores.get( turnoJugador );

        this.tablero.asignarPosesionCasilla( auxJugador, casillaNumero );
    }

    /**-------------------------------------------------------------------------
     * 
     * @param casillaNumero : Número de la casilla por la que se quiere hacer
     *                        la consulta.
     *
     * @return el precio de compra de la casilla "casillaNumero".
     */
    public int obtenerPrecioCompraCasilla( int casillaNumero )
    {
        return tablero.obtenerPrecioCompraEnCasilla( casillaNumero );
    }

    /**------------------------------------------------------------------------
     * 
     */
    public void establecerTurnoSiguiente()
    {
        this.turnoJugador = ( int )( ( turnoJugador + 1 ) % listaJugadores.size() );
    }

    /**-------------------------------------------------------------------------
     * 
     */
    public void eliminarJugadorConTurno()
    {
        /* Borramos al jugador de la casilla en la que se encuentra */
        Jugador auxJugador = listaJugadores.get( turnoJugador );
        this.tablero.quitarJugadorDeCasilla( auxJugador );
        
        /* Eliminamos al jugador de la lista de jugadores */
        listaJugadores.remove( turnoJugador );
    }

    /**-------------------------------------------------------------------------
     *
     * @return el número de jugadores que hay en el juego actual.
     */
    public int obtenerNumeroDeJugadores()
    {
        return( ( int )listaJugadores.size() );
    }

    /**-------------------------------------------------------------------------
     *
     */
    public void devolverAlJuegoPosesionesJugadorConTurno()
    {
        Jugador auxJugador = listaJugadores.get( turnoJugador );

        for( int i = 1; i < this.tablero.obtenerTamanoTablero(); i++ )
        {
            Jugador jugadorEnPosesionAux = tablero.obtenerJugadorEnPosesion( i );

            if( ( jugadorEnPosesionAux != null ) &&  jugadorEnPosesionAux.equals( auxJugador ) )
            {
                /* Devolvemos la posesión */
                tablero.devolverPosesion( i );
            }
        }
    }

    /**-------------------------------------------------------------------------
     *
     * @return La posición que ocupa en el tablero al juagador con turno.
     */
    public int obtenerPosicionJugadorConTurnoEnTablero()
    {
         Jugador auxJugador = listaJugadores.get( turnoJugador );
         return( auxJugador.obtenerPosicionEnTablero() );
    }

    /**-------------------------------------------------------------------------
     *
     * @return true si el juego está finalizado o false en caso contrario.
     */
    public boolean estaJuegoFinalizado()
    {
        return juegoFinalizado;
    }

    /**-------------------------------------------------------------------------
     *
     * @param juegoFinalizado : estabece si el juego esta o no finalizado.
     */
    public void establecerJuegoFinalizado( boolean juegoFinalizado )
    {
        this.juegoFinalizado = juegoFinalizado;
    }

    /**-------------------------------------------------------------------------
     *
     * @return true si el jugador posee indulto para la salida de la carcel,
     *         false en caso contrario.
     */
    public boolean estaJugadorConTurnoIndultado()
    {
        return( listaJugadores.get( turnoJugador ).tieneIndulto() );
    }

    /**-------------------------------------------------------------------------
     * 
     * @param indulto : establece si tiene o no tiene indulto el jugador con
     *                  turno actual.
     */
    public void establecerIndultoAJugadorConTurno( boolean indulto )
    {
        listaJugadores.get( turnoJugador ).establecerIndulto( indulto );
    }

    /**-------------------------------------------------------------------------
     * 
     * @param path : Ruta donde se guardará el juego actual.
     *
     * @return el código de error producido en la acción.
     */
    public int guardar( String path )
    {
        /* Creamos un nuevo objeto persistencia */
        Persistencia op = new Persistencia();

        /* Guardamos el juego */
        op.guardarJuego( path, this );

        /* Alamcenamos el valor del código de error producido en la acción */
        this.codigoErrorProducidoEnPersistencia = op.obtenerCodigoError();

        /* Retornamos el código de error producido en la acción */
        return( this.codigoErrorProducidoEnPersistencia );
    }

    /**-------------------------------------------------------------------------
     *
     * @param path : Ruta de donde se cargará el juego salvado. Si el valor
     *               obtenido es null siginifica que se ha producido un error
     *               11 en el proceso.
     * 
     */
    public Juego continuar( String path )
    {
        /* Creamos un nuevo objeto persistencia */
        Persistencia op    = new Persistencia();

        /* Recuperamo el objeto juego almacenado en disco */
        Juego        juego = op.continuarPartida( path );

        /* Alamcenamos el valor del código de error producido en la acción */
        this.codigoErrorProducidoEnPersistencia = op.obtenerCodigoError();

        /* Retornamos el juego recuperado */
        return juego;
    }   
}

