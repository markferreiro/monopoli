/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : Jugador.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que modela un jugador del monoply.
 *
 ******************************************************************************/

package applicationLayer;

import java.io.Serializable;

public class Jugador implements Serializable
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE INSTANCIA                                              |
    +------------------------------------------------------------------------*/
    private String  nombre;
    private String  color;
    private int    posicionEnTablero;
    private int     dinero;
    private boolean tieneIndulto;
    private boolean isPC;
    private int turnosEnCarcel;
   
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/
    /**
     *
     * @param nombre            : Nombre que se asignará al jugador.
     * @param i : Posición del tablero que ocupará el jugador.
     * @param dinero            : Dinero que se asignará al jugador.
     */
    public Jugador( String nombre, int i, int dinero )
    {
        this.nombre            = nombre;
        this.posicionEnTablero = i;
        this.dinero            = dinero;
        this.tieneIndulto      = false;
        this.isPC			   = (nombre.startsWith("PC") ? true : false);
        this.turnosEnCarcel	   = 0;
    }   

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICOS                                           |
    +------------------------------------------------------------------------*/

    /**-------------------------------------------------------------------------
     *
     * @return el nombre del jugador.
     */
    public String obtenerNombre() {        
        return this.nombre;
    }
    
    /**
     * 
     * @return TRUE si el jugador puede salir. FALSE si el jugador no puede salir.
     */
    public boolean dimeCarcel() {
    	boolean res = false;
    	
    	if (this.turnosEnCarcel < 3) {
    		this.turnosEnCarcel++;
    	} else {
    		this.turnosEnCarcel = 0;
    		res = true;
    	}
    	
    	return res;
    }

    /**-------------------------------------------------------------------------
     *
     * @return el color del jugador.
     */
    public String obtenerColor()
    {
        return this.color;
    }

    /**-------------------------------------------------------------------------
     * 
     * @param color :  establece el color del jugador.
     */
    public void establecerColor( String color )
    {
        this.color = color;
    }

    /**-------------------------------------------------------------------------
     *
     * @return la posición del tablero que ocupa el jugador.
     */
    public int obtenerPosicionEnTablero()
    {
        return this.posicionEnTablero;
    }

    /**-------------------------------------------------------------------------
     *
     * @return el dinero disponible que tiene el jugador.
     */
    public int obtenerDineroDisponible()
    {
        return this.dinero;
    }

    /**-------------------------------------------------------------------------
     *
     * @param posicionEnTablero : establece que posición en el tablero ocupa
     *                            el jugador.
     */
    public void establecerPosicionEnTablero( int posicionEnTablero )
    {
        this.posicionEnTablero = posicionEnTablero;
    }

    /**-------------------------------------------------------------------------
     * 
     * @param aumento : valor de dinero que se asignará al jugador.
     */
    public void establecerDinero( int cantidad )
    {
        this.dinero = cantidad;
    }

    /**-------------------------------------------------------------------------
     *
     * @param aumento : aumento del dinero actual del jugador.
     */
    public void aumentarDineroPorPago( int aumento )
    {
        this.dinero += aumento;
    }

    /**-------------------------------------------------------------------------
     *
     * @return true si el jugador dispone de indulto o false en caso contrario.
     */
    public boolean tieneIndulto()
    {
        return tieneIndulto;
    }

    /**-------------------------------------------------------------------------
     *
     * @param indulto : establece o no indulto para el jugador.
     */
    public void establecerIndulto( boolean indulto )
    {
        this.tieneIndulto = indulto;
    }
    public boolean esPC() {
    	return this.isPC;
    }
}
