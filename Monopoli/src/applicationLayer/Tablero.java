/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : Tablero.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que modela un tablero del monoply.
 *
 ******************************************************************************/

package applicationLayer;

import applicationLayer.casilla.Casilla;
import applicationLayer.casilla.CasillaCarcel;
import applicationLayer.casilla.CasillaExtendida;
import applicationLayer.casilla.CasillaLoteria;

import java.io.Serializable;
import java.util.ArrayList;

public class Tablero implements Serializable
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE INSTANCIA                                              |
    +------------------------------------------------------------------------*/
    private       int                 tamanoTablero;
    private       ArrayList< Casilla > tablero;   

    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     * 
     * @param tablero : Tablero que compone el juego.
     */
    public Tablero( ArrayList< Casilla > tablero )
    {        
        this.tablero       = tablero;
        this.tamanoTablero = tablero.size();
    }

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICO[S]                                         |
    +------------------------------------------------------------------------*/
    
    public ArrayList<Casilla> getTablero() {
		return tablero;
	}

	/**-------------------------------------------------------------------------
     *
     * @param casillaNumero: Ordinal de la casilla referenciada.
     * @return             : El tipo de la casilla.
     */
    public String obtenerTipoCasillaEnPosicion( int casillaNumero )
    {
        return tablero.get( casillaNumero ).obtenerTipo();
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero: Ordinal de la casilla referenciada.
     * @return             : La Categoría de la casilla.
     */
    public String obtenerCategoriaCasillaEnPosicion( int casillaNumero )
    {
        return tablero.get( casillaNumero ).obtenerCategoria();
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero: Ordinal de la casilla referenciada.
     * @return             : El coste por caer en la casilla.
     */
    public int obtenerCostePorCaerCasillaEnPosicion( int casillaNumero )
    {
        return tablero.get( casillaNumero ).obtenerCostePorCaer();
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero: Ordinal de la casilla referenciada.
     * @return             : El nombre del jugador que está en la casilla.
     */
    public ArrayList< Jugador > obtenerListaJugadoresEnCasilla( int casillaNumero )
    {
        return tablero.get( casillaNumero ).obtenerListaJugadoresEnCasilla();
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero
     * @return el precio de compra. -1 Si la casilla no es comprable.
     */
    public int obtenerPrecioCompraEnCasilla( int casillaNumero )
    {
        int returnValue;

        Casilla casilla = tablero.get( casillaNumero );       

        if( casilla instanceof CasillaCarcel || casilla instanceof CasillaLoteria )
        {
            returnValue = -1;
        }
        else
        {
            returnValue = ( ( CasillaExtendida )casilla ).obtenerPrecioDeCompra();
        }
        
        return returnValue;
    }

    /**-------------------------------------------------------------------------
     *
     * @param casillaNumero
     * @return El jugador que ha adquirido la casilla. null si la casilla no es comprable.
     */
    public Jugador obtenerJugadorEnPosesion( int casillaNumero )
    {
        Jugador returnValue;

        Casilla casilla = tablero.get( casillaNumero );

        if( casilla instanceof CasillaCarcel || casilla instanceof CasillaLoteria )
        {
            returnValue = null;
        }
        else
        {
            returnValue = ( ( CasillaExtendida )casilla ).obtenerJugadorEnPosesion();
        }

        return returnValue;
    }

    /**-------------------------------------------------------------------------
     *
     * @return el tamaño del tablero.
     */
    public int obtenerTamanoTablero()
    {
        return this.tamanoTablero;
    }

    /**-------------------------------------------------------------------------
     *
     * @param jugador a establecer.
     * @param casilla de destino.
     */
    public void establecerJugadorEnCasilla( Jugador jugador, int casilla )
    {
        tablero.get( casilla ).establecerJugador( jugador );
    }

    /**-------------------------------------------------------------------------
     * 
     * @param jugador a eliminar de la casilla.
     */
    public void quitarJugadorDeCasilla( Jugador jugador )
    {
        tablero.get( jugador.obtenerPosicionEnTablero() ).establecerQuitarJugador( jugador );
    }

    /**-------------------------------------------------------------------------
     * 
     * @param jugador que compra casilla.
     * @param casilla que es comprada.
     */
    public void asignarPosesionCasilla( Jugador jugador, int casilla )
    {
        Casilla auxCasilla                   = tablero.get( casilla );
        CasillaExtendida auxCasillaExtendida = ( CasillaExtendida )auxCasilla;

        auxCasillaExtendida.establecerPosesionAJugador( jugador );
    }

    /**-------------------------------------------------------------------------
     *
     * @param casilla a ser liberada.
     */
    public void devolverPosesion( int casilla )
    {
        Casilla          auxCasilla          = tablero.get( casilla );
        CasillaExtendida auxCasillaExtendida = ( CasillaExtendida )auxCasilla;

        auxCasillaExtendida.devolverPosesion();
    }
}


 

