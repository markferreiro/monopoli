/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : Casilla.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que modela una casilla padre. Esta implementación reune
 *             todos los aspectos comunes a todas las casillas necesarias para
 *             este juego.
 *
 ******************************************************************************/

package applicationLayer.casilla;

import applicationLayer.Jugador;
import java.io.Serializable;
import java.util.ArrayList;

public class Casilla implements Serializable
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE CLASE                                                  |
    +------------------------------------------------------------------------*/
    
    protected int                 posicion;
    protected String               tipo;
    protected String               categoria;
    protected int                  costePorCaer;
    protected ArrayList< Jugador > listaJugadoresEn;

    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/  

    /**-------------------------------------------------------------------------
     *
     * @param tipo      : Tipo de la casilla.
     * @param categoria : Categoría de la casilla.
     */
    public Casilla( String tipo, String categoria )
    {
        this.posicion         = 0;
        this.tipo             = tipo;
        this.categoria        = categoria;
        this.costePorCaer     = 0;
        this.listaJugadoresEn = new ArrayList< Jugador >();
    }

    /**-------------------------------------------------------------------------
     *
     * @param tipo         : Tipo de la casilla.
     * @param categoria    : Categoría de la casilla.
     * @param costePorCaer : Precio que hay que pagar por caer en la casilla.
     */
    public Casilla( String tipo, String categoria, int costePorCaer )
    {
        this( tipo, categoria );
        this.posicion         = 0;
        this.costePorCaer     = costePorCaer;
        this.listaJugadoresEn = new ArrayList< Jugador >();
    }   

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICOS                                           |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     *
     * @return la posicion que ocupa la casilla en el tablero.
     */
    public int obtenerPosicionEnTablero()
    {
        return this.posicion;
    }

    /**-------------------------------------------------------------------------
     *
     * @return el coste por caer en la casiilla.
     */
    public int obtenerCostePorCaer()
    {
        return this.costePorCaer;
    }    

    /**-------------------------------------------------------------------------
     *
     * @return el tipo de la casilla
     */
    public String obtenerTipo()
    {
        return tipo;
    }

    /**-------------------------------------------------------------------------
     *
     * @return la categoria de la casilla
     */
    public String obtenerCategoria()
    {
        return this.categoria;
    }

    /**-------------------------------------------------------------------------
     *
     * @return una lista de jugadores que hay actualmente en la casilla.
     */
    public ArrayList< Jugador > obtenerListaJugadoresEnCasilla()
    {
        return this.listaJugadoresEn;
    }

    /**-------------------------------------------------------------------------
     *
     * @param posicion : establece la posición que ocpará en el tablero.
     */
    public void establecerPosicionEnTablero( int posicion )
    {
        this.posicion = posicion;
    }

    /**-------------------------------------------------------------------------
     *
     * @param costePorCaer: establece el coste por caer en la casilla.
     */
    public void establecerCostePorCaer( int costePorCaer )
    {
        this.costePorCaer = costePorCaer;
    }

    /**-------------------------------------------------------------------------
     * 
     * @param jugador: Jugador que actualmente está en la casilla
     */
    public void establecerJugador( Jugador jugador )
    {
        this.listaJugadoresEn.add( jugador );
    }

    /**-------------------------------------------------------------------------
     * 
     * @param jugador : Jugador a eliminar de la casilla.
     */
    public void establecerQuitarJugador( Jugador jugador )
    {
        this.listaJugadoresEn.remove( jugador );
    }
}
