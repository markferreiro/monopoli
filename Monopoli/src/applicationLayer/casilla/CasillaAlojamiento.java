/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaAlojamiento.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaExtendida. Esta implementación reune
 *             los aspectos necesarios a las casillas de tipo Alojamiento.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaAlojamiento extends CasillaExtendida
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**-------------------------------------------------------------------------
     *
     * @param categoria            : Categoria de la casilla.
     * @param precioMinimoDeCompra : Precio mínimo de compra de la casilla.
     * @param precioMaximoDeCompra : Precio máximo de compra de la casilla.
     */
    public CasillaAlojamiento( String categoria, int precioMinimoDeCompra, int precioMaximoDeCompra )
    {
        super( ICasilla.ALOJAMIENTO, categoria, precioMinimoDeCompra, precioMaximoDeCompra );
    }
}
