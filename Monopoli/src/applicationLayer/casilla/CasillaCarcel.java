/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaCarcel.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaEspecial. Esta implementación reune
 *             los aspectos necesarios a las casillas de tipo Carcel.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaCarcel extends CasillaEspecial
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/
   
    public CasillaCarcel()
    {
        super( ICasilla.CARCEL, 0 );
    }
}
