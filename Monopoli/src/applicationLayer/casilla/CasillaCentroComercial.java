/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaCentroComercial.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaServicioPrivado. Esta implementación 
 *             reune los aspectos necesarios a las casillas de tipo Centro
 *             Comercial.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaCentroComercial extends CasillaServicioPrivado
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**
     * Constructor de la clase CasillaCentroComercial.
     */
    public CasillaCentroComercial()
    {
        super( ICasilla.CENTRO_COMERCIAL, 10000, 20000 );
    }
}
