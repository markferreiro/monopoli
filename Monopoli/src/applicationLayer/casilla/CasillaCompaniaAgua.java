/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaCompaniaAgua.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaServicioPublico. Esta implementación
 *             reune los aspectos necesarios de las casillas de tipo Compañía
 *             del Agua.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaCompaniaAgua extends CasillaServicioPublico
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    public CasillaCompaniaAgua()
    {
        super( ICasilla.COMPANIA_AGUA );
    }
}
