/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaCompaniaLuz.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaServicioPublico. Esta implementación
 *             reune los aspectos necesarios de las casillas de tipo Compañía
 *             de la Luz.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaCompaniaLuz extends CasillaServicioPublico
{
     /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**
     * Constructor de la clase CasillaCompaniaAgua.
     */
    public CasillaCompaniaLuz()
    {
        super( ICasilla.COMPANIA_LUZ );
    }
}