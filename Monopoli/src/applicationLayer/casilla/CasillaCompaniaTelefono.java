/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaCompaniaTelefono.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaServicioPublico. Esta implementación
 *             reune los aspectos necesarios de las casillas de tipo Compañía
 *             del Teléfono.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaCompaniaTelefono extends CasillaServicioPublico
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/
    
    public CasillaCompaniaTelefono()
    {
        super( ICasilla.COMPANIA_TELEFONO );
    }
}