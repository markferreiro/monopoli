/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaEdificio.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaAlojamiento. Esta implementación reune
 *             los aspectos necesarios de las casillas de tipo Edificio.
 *
 ******************************************************************************/

package applicationLayer.casilla;

/**
 *
 * @author 
 */
public class CasillaEdificio extends CasillaAlojamiento
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**
     * Constructor de la clase CasillaEdificio.
     */
    public CasillaEdificio()
    {
        super( ICasilla.EDIFICIO, 5000, 10000 );
    }
}
