/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaEspecial.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de Casilla. Esta implementación reune los
 *             aspectos necesarios de las casillas de tipo Casilla Especial.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaEspecial extends Casilla
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**
     *
     * @param categoria    : Establece la categoría de la casilla.
     * @param costePorCaer : Establece el precio a pagar por caer en la casilla.
     */
    public CasillaEspecial( String categoria, int costePorCaer )
    {
        super( ICasilla.ESPECIAL, categoria, costePorCaer );
    }
}
