/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaExtendida.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de Casilla. Esta implementación reune los
 *             aspectos necesarios de las casillas de tipo Casilla Extendida.
 *
 ******************************************************************************/

package applicationLayer.casilla;

import applicationLayer.Jugador;
import java.util.Random;

public class CasillaExtendida extends Casilla
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE INSTANCIA                                              |
    +------------------------------------------------------------------------*/
    private int     precioMinimoDeCompra;
    private int     precioMaximoDeCompra;
    private int     precioDeCompra;
    private Jugador adquiridaPorJugador;

    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     *
     * @param tipo                 : Establece el tipo de la casilla.
     * @param categoria            : Establece la categoría de la casilla.
     * @param precioMinimoDeCompra : Establece el precio mínimo de compra.
     * @param precioMaximoDeCompra : Establece el precio máximo de compra.
     */
    public CasillaExtendida( String tipo, String categoria, int precioMinimoDeCompra, int precioMaximoDeCompra )
    {
        super( tipo, categoria );

        this.precioMinimoDeCompra = precioMinimoDeCompra;
        this.precioMaximoDeCompra = precioMaximoDeCompra;
        this.precioDeCompra       = this.calcularPrecioDeCompra();
        this.establecerCostePorCaer( this.calcularCostePorCaer() );
        this.adquiridaPorJugador  = null;
    }   

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICO[S]                                         |
    +------------------------------------------------------------------------*/

    /**-------------------------------------------------------------------------
     *
     * @return el precio de compra de la casilla.
     */
    public int obtenerPrecioDeCompra()
    {
        return this.precioDeCompra;
    }

    /**-------------------------------------------------------------------------
     *
     * @return el jugador que adquirió la casilla.
     */
    public Jugador obtenerJugadorEnPosesion()
    {
        return this.adquiridaPorJugador;
    }

   /**--------------------------------------------------------------------------
    *
    * @param jugador : Jugador al que se asigna la posesión.
    */
    public void establecerPosesionAJugador( Jugador jugador )
    {
        this.adquiridaPorJugador = jugador;
    }

    /**-------------------------------------------------------------------------
     *
     */
    public void devolverPosesion()
    {
        adquiridaPorJugador = null;
    }

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PRIVADO[S]                                         |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     * 
     * @param precioMinimoDeCompra : Precio mínimo de compra.
     * @param precioMaximoDeCompra : Precio máximo de compra.
     * @return El precio de compra de la casilla.
     */
    private int calcularPrecioDeCompra()
    {
        int    START  = this.precioMinimoDeCompra;
        int    END    = this.precioMaximoDeCompra;
        Random random = new Random();

        if( START > END )
        {
            /* Este comportamiento es anómalo, se asume que estan intercambiados */
            int aux   = START;
                START = END;
                END   = START;
        }

        /*
         * Parte de código sacado de:
         *
         * http://www.javapractices.com/topic/TopicAction.do?Id=62
         *
         * EJEMPLO 2
         * 
         */

        long range = ( long )END - ( long )START + 1;

        /* compute a fraction of the range, 0 <= frac < range */
        long fraction     = ( long )( range * random.nextDouble() );
        int  randomNumber = ( int )( fraction + START );

        return randomNumber;
    }

    /**-------------------------------------------------------------------------
     * 
     * @param precioDeCompra : Precio de compra de la casilla.
     * @return El precio por caer en la casilla.
     */
    private int calcularCostePorCaer()
    {
        return( ( this.precioDeCompra * 10 ) / 100 );
    }
}
