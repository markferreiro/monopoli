/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaHotel.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaServicioPrivado. Esta implementación
 *             reune los aspectos necesarios de las casillas de tipo Hotel.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaHotel extends CasillaServicioPrivado
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    public CasillaHotel()
    {
        super( ICasilla.HOTEL, 5000, 10000 );
    }
}
