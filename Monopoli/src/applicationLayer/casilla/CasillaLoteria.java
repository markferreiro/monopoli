/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaLoteria.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaEspecial. Esta implementación
 *             reune los aspectos necesarios de las casillas de tipo Lotería.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaLoteria extends CasillaEspecial {
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/ 
    
    public CasillaLoteria() {
        super( ICasilla.LOTERIA, 100 );
    }
    
}
