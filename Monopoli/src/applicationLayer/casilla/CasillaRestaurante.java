/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaRestaurante.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaServicioPrivado. Esta implementación
 *             reune los aspectos necesarios de las casillas de tipo Restaurante.
 *
 ******************************************************************************/

package applicationLayer.casilla;

/**
 *
 * @author
 */
public class CasillaRestaurante extends CasillaServicioPrivado
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**
     * Constructor de la clase CasillaHotel.
     */
    public CasillaRestaurante()
    {
        super( ICasilla.RESTAURATE, 500, 1000 );
    }
}

