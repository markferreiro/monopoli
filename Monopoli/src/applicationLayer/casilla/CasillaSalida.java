/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaSalida.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaEspecial. Esta clase es únicamente
 *             válida al comienzo de una partida y es útil para reunir a todos
 *             los jugadores en un punto de inicio común.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaSalida extends CasillaEspecial
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    public CasillaSalida()
    {
        super( ICasilla.SALIDA, 0 );
    }
}
