/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaServicioPrivado.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaExtendida. Esta implementación reune
 *             los aspectos necesarios de las casillas de tipo Servicio Privado.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaServicioPrivado extends CasillaExtendida
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    public CasillaServicioPrivado( String categoria, int precioMinimoDeCompra, int precioMaximoDeCompra )
    {
        super( ICasilla.SERVICIO_PRIVADO, categoria, precioMinimoDeCompra, precioMaximoDeCompra );
    }
}
