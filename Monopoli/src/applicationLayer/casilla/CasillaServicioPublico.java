/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaServicioPublico.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaExtendida. Esta implementación reune
 *             los aspectos necesarios de las casillas de tipo Servicio Publico.
 *
 ******************************************************************************/

package applicationLayer.casilla;

/**
 *
 * @author 
 */
public class CasillaServicioPublico extends CasillaExtendida
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**
     *
     */
    public CasillaServicioPublico( String categoria )
    {
        super( ICasilla.SERVICIO_PUBLICO, categoria, 100, 500 );
    }

}
