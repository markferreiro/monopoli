/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : CasillaUnifamiliar.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que hereda de CasillaAlojamiento. Esta implementación reune
 *             los aspectos necesarios de las casillas de tipo Casilla
 *             unifamiliar.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public class CasillaUnifamiliar extends CasillaAlojamiento
{
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    public CasillaUnifamiliar()
    {
        super( ICasilla.CASA_UNIFAMILIAR, 500, 1000 );
    }
}
