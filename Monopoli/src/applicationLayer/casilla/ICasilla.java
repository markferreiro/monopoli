/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : ICasilla.java
 * VERSION   : 1.0
 * DEFINICIÓN: Interface que únicamente implementa constantes para aportar
 *             claridad en la creación de los distintos tipos de casillas.
 *
 ******************************************************************************/

package applicationLayer.casilla;

public interface ICasilla
{
    /*------------------------------------------------------------------------+
    |   CONSTANTES[S]                                                         |
    +------------------------------------------------------------------------*/
    
    final static String ALOJAMIENTO       = "Alojamiento";
    final static String SERVICIO_PUBLICO  = "Servicio publico";
    final static String SERVICIO_PRIVADO  = "Servicio privado";
    final static String ESPECIAL          = "Especial";
    final static String CASA_UNIFAMILIAR  = "Casa unifamiliar";
    final static String EDIFICIO          = "Edificio";
    final static String COMPANIA_AGUA     = "Compania del agua";
    final static String COMPANIA_GAS      = "Compania del gas";
    final static String COMPANIA_LUZ      = "Compania de la Luz";
    final static String COMPANIA_TELEFONO = "Compania del telefono";
    final static String CENTRO_COMERCIAL  = "Centro comercial";
    final static String HOTEL             = "Hotel";
    final static String RESTAURATE        = "Restaurante";
    final static String CARCEL            = "Carcel";
    final static String LOTERIA           = "Loteria";    
    final static String SALIDA            = "Salida";
}
