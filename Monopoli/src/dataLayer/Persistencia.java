/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : Persistencia.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que sirve para resolver los problemas relacionados con la
 *             persistencia del juego. 
 *
 ******************************************************************************/

package dataLayer;

import applicationLayer.Juego;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Persistencia
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE CLASE                                                  |
    +------------------------------------------------------------------------*/
    private String path;
    private byte   codigoError;

    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    public Persistencia()
    {
        this.path        = null;
        this.codigoError = 0;
    }

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICO[S]                                         |
    +------------------------------------------------------------------------*/

    /**-------------------------------------------------------------------------
     *
     * @param path  : Ruta en donde será almacenado el juego.
     * @param juego : Juego que será almacenado en disco.
     */
    public void guardarJuego( String path, Juego juego )
    {
        /* Aun no se ha producido ningun error de nivel */
        this.codigoError = 0;

        /* Declaramos los objetos necesarios */
        FileOutputStream   fos = null;
        ObjectOutputStream oos = null;

        try
        {
            /* Creamos un flujo de salida */
            fos = new FileOutputStream( path + "\\monopoly.sav" );
        }
        catch( FileNotFoundException ex )
        {
            this.codigoError = 10;
        }

        if( this.codigoError == 0 )
        {
            try
            {
                /* Continuamos vinculando el flujo de salida al objeto fos */
                oos = new ObjectOutputStream( fos );
            }
            catch( IOException ex )
            {
                this.codigoError = 8;
            }
        }

        if( this.codigoError == 0 )
        {
            try
            {
                /* Serializamos el objeto al disco */
                oos.writeObject( juego );
            }
            catch( IOException ex )
            {
                this.codigoError = 8;
            }
        }

        if( this.codigoError == 0 )
        {
            try
            {
                /* Por último, cerramos el flujo */
                oos.close();
            }
            catch( IOException ex )
            {
                this.codigoError = 8;
            }
        }
    }

    /**-------------------------------------------------------------------------
     *
     * @param path : Ruta de donde será recuperado un juego salvado.
     * @return el juego salvado en disco.
     */
    public Juego continuarPartida( String path )
    {
        /* Creamos un objeto juego vacío */
        Juego juego = null;

        /* Aun no se ha producido ningun error de nivel */
        this.codigoError = 0;

        /* Declaramos los objetos necesarios */
        FileInputStream   fis = null;
        ObjectInputStream ois = null;

        try
        {
            /* Creamos un flujo de salida */
            fis = new FileInputStream( path + "\\monopoly.sav" );
        }
        catch( FileNotFoundException ex )
        {
            this.codigoError = 11;
        }

        if( this.obtenerCodigoError() == 0 )
        {
            try
            {
                /* Continuamos vinculando el flujo de salida al objeto fos */
                ois = new ObjectInputStream( fis );
            }
            catch( IOException ex )
            {
                this.codigoError = 8;
            }
        }

        if( this.obtenerCodigoError() == 0 )
        {
            try
            {
                /* Serializamos el objeto al disco */                
                juego = ( Juego )ois.readObject();
            }
            catch( IOException ex )
            {
                this.codigoError = 8;
            }
            catch( ClassNotFoundException ex )
            {
                this.codigoError = 8;
            }
        }

        if( this.obtenerCodigoError() == 0 )
        {
            try
            {
                /* Por último, cerramos el flujo */
                ois.close();
            }
            catch( IOException ex )
            {
                this.codigoError = 8;
            }
        }

        return juego;
    }
    
    /**-------------------------------------------------------------------------
     *
     * @return el últmo código de error producido en alguna de las acciones de
     *         persistencia. Estos código son los definidos en la clase 
     */
    public byte obtenerCodigoError()
    {
        return this.codigoError;
    }
}
