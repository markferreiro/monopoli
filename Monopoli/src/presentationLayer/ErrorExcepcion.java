/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : ErrorExcepcion.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase utilitaria que ayuda a mensajear por consloa errores y/o
 *             excepciones producidas durante la vida del programa.
 *
 ******************************************************************************/

package presentationLayer;

public class ErrorExcepcion
{
    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICOS                                           |
    +------------------------------------------------------------------------*/

    /**
     * Muestra información acerca del error producido.
     * @param errorExcepcion: código del error.
     */
    public static void mostarInfo( int errorExcepcion )
    {
        switch( errorExcepcion )
        {
            case 1:
                Utils.imprimirTextoFormateado( "** ERROR: El valor ha de estar comprendido entre 1 y 3.", false ) ;
                break;
             case 2:
                Utils.imprimirTextoFormateado( "** ERROR: El valor ha de estar comprendido entre 2 y 5.", false ) ;
                break;
             case 3:
                Utils.imprimirTextoFormateado( "** ERROR: El valor ha de estar comprendido entre 20 y 100.", false ) ;
                Utils.imprimirTextoFormateado( "   El número total de casillas debera ser divisible entre 10.", false);
                break;
             case 4:
                Utils.imprimirTextoFormateado( "** ERROR: Nombre en uso.", false ) ;
                break;
             case 5:
                Utils.imprimirTextoFormateado( "** ERROR: Nombre demasiado largo, máximo 10 caracteres.", false ) ;
                break;
             case 6:
                Utils.imprimirTextoFormateado( "** ERROR: Color no disponible. Seleccione otro color.", false ) ;
                break;
            case 7:
                Utils.imprimirTextoFormateado( "** ERROR: Opción seleccionada no reconocible.", false ) ;
                break;
             case 8:
                /* Control de la excepción IOException */
                Utils.imprimirTextoFormateado( "** ERROR: Excepción de entrada/salida.", false ) ;
                break;
            case 9:
                Utils.imprimirTextoFormateado( "** ERROR: El valor ha de estar comprendido entre 1 y 4.", false ) ;
                break;
            case 10:
                /* Control de la excepción FileNotFoundException e IOException */
                Utils.imprimirTextoFormateado( "** ERROR: No se puede crear el fichero.", false ) ;
                Utils.imprimirTextoFormateado( "Asegurate que la ruta es correcta.", false ) ;
                break;
            case 11:
                /* Control de la excepción FileNotFoundException, IOException y ClassNotFoundException */
                Utils.imprimirTextoFormateado( "** ERROR: No se puede cargar el fichero.", false ) ;
                Utils.imprimirTextoFormateado( "Asegurate que la ruta es correcta.", false ) ;
                break;
            default:
                /* Error no contemplado */
                break;
        }

        /* Se espera la aceptación */
        Utils.presionarIntroParaContinuar();
    }
}
