package presentationLayer;

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Gif extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<ImageIcon> imatges;
	
	public Gif(ArrayList<ImageIcon> imgs, String txt) {
		// TODO Auto-generated constructor stub
		super(txt);
		this.imatges = imgs;
	}
	
	public void run(int milis) {
		int waiting = milis / this.imatges.size();
		for (ImageIcon image : imatges) {
			Image img = image.getImage();
			super.setIcon(new ImageIcon(img.getScaledInstance(super.getWidth(), super.getHeight(), Image.SCALE_SMOOTH)));
			try {
				System.out.println("Icono canviado. Sleep.");
				Thread.sleep(waiting);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
