package presentationLayer;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JNuevoJuego extends JDialog {
	public static Integer jugadores = 0;
	private JLabel label;

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			JNuevoJuego dialog = new JNuevoJuego();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public JNuevoJuego() {
		setBounds(100, 100, 194, 341);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JButton btnNewButton = new JButton("1");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					JNuevoJuego.jugadores = 1;
					label.setText(JNuevoJuego.jugadores.toString());
				}
			});
			btnNewButton.setFont(new Font("Segoe Marker", Font.BOLD, 28));
			btnNewButton.setBounds(10, 69, 75, 56);
			btnNewButton.setActionCommand("OK");
			contentPanel.add(btnNewButton);
		}
		{
			JButton button = new JButton("2");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JNuevoJuego.jugadores = 2;
					label.setText(JNuevoJuego.jugadores.toString());
				}
			});
			button.setFont(new Font("Segoe Marker", Font.BOLD, 28));
			button.setBounds(95, 69, 75, 56);
			button.setActionCommand("OK");
			contentPanel.add(button);
		}
		{
			JButton button = new JButton("3");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JNuevoJuego.jugadores = 3;
					label.setText(JNuevoJuego.jugadores.toString());
				}
			});
			button.setFont(new Font("Segoe Marker", Font.BOLD, 28));
			button.setBounds(10, 136, 75, 56);
			button.setActionCommand("OK");
			contentPanel.add(button);
		}
		{
			JButton button = new JButton("4");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JNuevoJuego.jugadores = 4;
					label.setText(JNuevoJuego.jugadores.toString());
				}
			});
			button.setFont(new Font("Segoe Marker", Font.BOLD, 28));
			button.setBounds(95, 136, 75, 56);
			button.setActionCommand("OK");
			contentPanel.add(button);
		}
		{
			JButton button = new JButton("5");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JNuevoJuego.jugadores = 5;
					label.setText(JNuevoJuego.jugadores.toString());
				}
			});
			button.setFont(new Font("Segoe Marker", Font.BOLD, 28));
			button.setBounds(55, 203, 75, 56);
			button.setActionCommand("OK");
			contentPanel.add(button);
		}
		
		JLabel lblJugadores = new JLabel("JUGADORES");
		lblJugadores.setHorizontalAlignment(SwingConstants.CENTER);
		lblJugadores.setFont(new Font("Tekton Pro", Font.BOLD, 17));
		lblJugadores.setBounds(31, 11, 113, 24);
		contentPanel.add(lblJugadores);
		
		label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(61, 30, 57, 29);
		contentPanel.add(label);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton button = new JButton("Ok");
				button.setActionCommand("Ok");
				buttonPane.add(button);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
}
