package presentationLayer;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import applicationLayer.Executor;
import applicationLayer.Juego;
import applicationLayer.Jugador;
import applicationLayer.Tablero;
import applicationLayer.casilla.Casilla;
import applicationLayer.casilla.CasillaCarcel;
import applicationLayer.casilla.CasillaCentroComercial;
import applicationLayer.casilla.CasillaCompaniaAgua;
import applicationLayer.casilla.CasillaCompaniaGas;
import applicationLayer.casilla.CasillaCompaniaLuz;
import applicationLayer.casilla.CasillaCompaniaTelefono;
import applicationLayer.casilla.CasillaEdificio;
import applicationLayer.casilla.CasillaHotel;
import applicationLayer.casilla.CasillaLoteria;
import applicationLayer.casilla.CasillaRestaurante;
import applicationLayer.casilla.CasillaSalida;
import applicationLayer.casilla.CasillaUnifamiliar;

public class JStartGame extends JFrame {

	private JPanel contentPane;
	private static int     opcionSeleccionada;
    private static int    errorExcepcion = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JStartGame frame = new JStartGame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JStartGame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 597, 438);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNuevaPartida = new JButton("Nueva partida");
		btnNuevaPartida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				crearNuevoJuego();
			}
		});
		btnNuevaPartida.setFont(new Font("Segoe Marker", Font.BOLD, 20));
		btnNuevaPartida.setBounds(188, 233, 212, 46);
		contentPane.add(btnNuevaPartida);
		
//		JButton btnContinuarPartida = new JButton("Continuar partida");
//		btnContinuarPartida.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				continuarPartida();
//			}
//		});
//		btnContinuarPartida.setFont(new Font("Segoe Marker", Font.BOLD, 20));
//		btnContinuarPartida.setBounds(188, 259, 212, 46);
//		contentPane.add(btnContinuarPartida);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setFont(new Font("Segoe Marker", Font.BOLD, 20));
		btnSalir.setBounds(188, 290, 212, 46);
		contentPane.add(btnSalir);
		
		JLabel lblNewLabel = new JLabel("");
		ImageIcon icon = new ImageIcon(JStartGame.class.getResource("/imgs/monopoly.jpg"));
		Image img = icon.getImage();
//		BufferedImage bi = new BufferedImage(lblNewLabel.getWidth(), lblNewLabel.getHeight(), BufferedImage.TYPE_INT_ARGB); 
//		Graphics g = bi.createGraphics(); 
//		g.drawImage(img, 0, 0, WIDTH, HEIGHT, null); 
//		ImageIcon newIcon = new ImageIcon(bi); 
		lblNewLabel.setBounds(10, 11, 561, 180);
		contentPane.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon(img.getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_SMOOTH)));
	}
	
	private static void crearNuevoJuego()
    {
        int                  nJugadores     = 0;
        int                  tamanoTablero  = 20;
        ArrayList< Jugador > listaJugadores = new ArrayList< Jugador >();
        Tablero              tablero;

        /* 1º: Solicitud del número de jugadores */
        nJugadores = obtenerNumeroJugadores();

        /* 1.a: Solicitud del nombre del jugador */
        obtenerNombresJugadores( nJugadores, listaJugadores );

         /* 1.b: Solicitud del color */
        obtenerColoresJugadores( nJugadores, listaJugadores );

//        /* 2º: Solicitud del tamaño del tablero */
//        tamanoTablero = obtenerTamanoTablero();

        /* 3º: Generación del tablero */
        tablero = generarTablero( tamanoTablero );
        
        for (Jugador jugador : listaJugadores) {
//        	tablero.getTablero().get(0).establecerJugador(jugador);
        	jugador.establecerPosicionEnTablero(0);
		}
        /* Se genera un nuevo Juego */
        Juego juego = new Juego( listaJugadores, tablero );
        
        String mensaje = "Jugadores: " + nJugadores + "\n";
        for (int i = 0; i <= listaJugadores.size()-1; i++) {
			mensaje += "Jugador " + (i+1) + ": " + listaJugadores.get(i).obtenerNombre() 
					+ " Color: " + listaJugadores.get(i).obtenerColor() + "\n";
		}

        Executor ejecutor = new Executor(juego);

    }
	private static int obtenerNumeroJugadores()
    {
        /* En primera instancia no existen jugadores */
        int nJugadores;
        
        Object stringArray[] = { "1", "2", "3", "4", "5" };
        nJugadores = JOptionPane.showOptionDialog(null, "Selecciona numero de jugadors:", "Nombre de jugadors",
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, stringArray,
            stringArray[0]);
        return nJugadores+1;
    }
	private static void obtenerNombresJugadores( int nJugadores, ArrayList< Jugador > listaJugadores )
    {
        String nombre;

        for( int i = 0; i < nJugadores; i++ )
        {
        	nombre = JOptionPane.showInputDialog("Introdueix el nom del jugador numero " + i+1);
            /* Creamos un objeto jugador con su nombre, la posición de salida y 10000 euros */
            Jugador auxJugador = new Jugador( nombre, 0, 10000 );
            /* Lo alamcenamos el la lista de jugadores */
            listaJugadores.add( auxJugador );
        }
    }
	private static void obtenerColoresJugadores( int nJugadores, ArrayList< Jugador > listaJugadores )
    {
		String stringArray[] = { "azul", "negro", "rojo", "amarillo", "verde" };
		Integer color;
		for( int i = 0; i < nJugadores; i++ )
        {
        	color = JOptionPane.showOptionDialog(null, "Selecciona color del jugadors:", "Nombre de jugadors",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, stringArray,
                    stringArray[0]);

            listaJugadores.get( i ).establecerColor(stringArray[color]);
        }
    }
	private static Tablero generarTablero( int tamanoTablero )
    {
        ArrayList< Casilla > tablero           = new ArrayList< Casilla >();
        int                 nCasillasLoteria  = ( tamanoTablero / 10 );
        int                 nCasillasCarcel   = ( tamanoTablero / 10 );
        int                 casillasrestantes = ( tamanoTablero - ( nCasillasCarcel + nCasillasLoteria ) );

        /* Añadimos las casillas cárceles */
        for( int i = 0; i < nCasillasCarcel; i++ )
        {
            tablero.add( new CasillaCarcel() );
        }

        /* Añadimos las casillas loterías al tablero */
        for( int i = 0; i < nCasillasLoteria; i++ )
        {
            tablero.add( new CasillaLoteria() );
        }

        /* Añadimos el resto de casillas de manera equitativa + ó - */
        for( int i = 0; i < casillasrestantes; i++ )
        {            
        	System.out.println("A�adimos casilla!");
            switch( i % 9 )
            {
                case 0:
                    tablero.add( new CasillaUnifamiliar() );
                    break;
                case 1:
                    tablero.add( new CasillaEdificio() );
                    break;
                case 2:
                    tablero.add( new CasillaCompaniaAgua() );
                    break;
                case 3:
                    tablero.add( new CasillaCompaniaGas() );
                    break;
                case 4:
                    tablero.add( new CasillaCompaniaLuz() );
                    break;
                case 5:
                    tablero.add( new CasillaCompaniaTelefono() );
                    break;
                case 6:
                    tablero.add( new CasillaCentroComercial() );
                    break;
                case 7:
                    tablero.add( new CasillaHotel() );
                    break;
                case 8:
                    tablero.add( new CasillaRestaurante() );
                    break;
                default:
                    break;
            }
        }

        /* Se mezclan las casillas aleatoriamente un número aleatorio de veces */
        for(  int i = 0; i < ( ( System.currentTimeMillis() % 9 ) + 1 ); i++ )
        {
            Collections.shuffle( tablero );
        }

        /* Añadimos al principio de la lista la casilla 0 que será la de salida */
        tablero.add( 0, new CasillaSalida() );

        /* Asignamos a cada casilla la posición que le corresponde en el tablero */
        for( int i = 0; i < tablero.size(); i++ )
        {
            tablero.get( i ).establecerPosicionEnTablero( ( i ) );
        }

        /* Retornamos el tablero */
        return( new Tablero( tablero ) );
    }
}
