package presentationLayer;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import applicationLayer.Executor;
import applicationLayer.Juego;
import applicationLayer.Jugador;

public class JTablero extends JFrame {
	private static ArrayList<JLabel> lblArray;
	private static ArrayList<JPanelTitle> panelArray;
	private JPanelTitle contentPane, panel11, panel12, panel13, panel14, panel10, panel8, panel15, panel16, panel9, panel5, panel4,
	panel3, panel2, panel20, panel17, panel18, panel7, panel19, panel1, panel6, panel1p11, panel2p11, panel3p11,
	panel5p11, panel4p11, panel1p12, panel2p12, panel3p12, panel5p12, panel4p12, panel1p13, panel2p13, panel3p13, panel5p13, panel4p13,
	panel1p14, panel2p14, panel3p14, panel5p14, panel4p14, panel1p15, panel2p15, panel3p15, panel5p15, panel4p15, panel1p16, panel2p16,
	panel3p16, panel5p16, panel4p16, panel1p17, panel2p17, panel3p17, panel5p17, panel4p17, panel1p18, panel2p18, panel3p18, panel5p18, 
	panel4p18, panel1p19, panel2p19, panel3p19, panel5p19, panel4p19, panel1p20, panel2p20, panel3p20, panel5p20, panel4p20, panel1p1,
	panel2p1, panel3p1, panel5p1, panel4p1, panel1p2, panel2p2, panel3p2, panel5p2, panel4p2, panel1p3, panel2p3, panel3p3,
	panel5p3, panel4p3, panel1p4, panel2p4, panel3p4, panel5p4, panel4p4, panel1p5, panel2p5, panel3p5, panel5p5, panel4p5,
	panel1p6, panel2p6, panel3p6, panel5p6, panel4p6, panel1p7, panel2p7, panel3p7, panel5p7, panel4p7, panel1p8,
	panel2p8, panel3p8, panel5p8, panel4p8, panel1p9, panel2p9, panel3p9, panel5p9, panel4p9, panel1p10, panel2p10,
	panel3p10, panel5p10, panel4p10;
	private JLabel lblp1, lblp2, lblp3, lblp4, lblp5, lblp6, lblp7, lblp8, lblp9, lblp10, lblp11, lblp12, lblp13, lblp14, lblp15, 
	lblp16, lblp17, lblp18, lblp19, lblp20;
	public static JLabel lblInstrucciones, lblBackground, lblGif;
	public static JButton btnStartGame;
	private static ArrayList<ImageIcon> dados;
	public static boolean isReady = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JTablero frame = new JTablero();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JTablero() {
		JTablero.dados = new ArrayList<ImageIcon>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 815, 647);
		contentPane = new JPanelTitle();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnStartGame = new JButton("Iniciar juego");
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnStartGame.getText().contains("Tirar")) {
					Executor.haTiradoJugadorElDado = true;
				}
				btnStartGame.setVisible(false);
				try {
					Executor.iniciaElJuego();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnStartGame.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnStartGame.setBounds(489, 329, 108, 31);
		contentPane.add(btnStartGame);

		lblInstrucciones = new JLabel("Pulsa el botón Iniciar Juego para iniciar la partida.");
		lblInstrucciones.setHorizontalAlignment(SwingConstants.CENTER);
		lblInstrucciones.setBounds(192, 137, 416, 25);
		contentPane.add(lblInstrucciones);

		panel11 = new JPanelTitle();
		panel11.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel11.setBounds(73, 38, 108, 88);
		contentPane.add(panel11);
		panel11.setLayout(null);

		lblp11 = new JLabel("");
		lblp11.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp11.setHorizontalAlignment(SwingConstants.CENTER);
		lblp11.setBounds(10, 0, 88, 47);
		panel11.add(lblp11);

		panel1p11 = new JPanelTitle();
		panel1p11.setBounds(10, 58, 18, 19);
		panel11.add(panel1p11);

		panel2p11  = new JPanelTitle();
		panel2p11.setBounds(27, 58, 18, 19);
		panel11.add(panel2p11);

		panel3p11 = new JPanelTitle();
		panel3p11.setBounds(44, 58, 18, 19);
		panel11.add(panel3p11);

		panel5p11 = new JPanelTitle();
		panel5p11.setBounds(80, 58, 18, 19);
		panel11.add(panel5p11);

		panel4p11 = new JPanelTitle();
		panel4p11.setBounds(63, 58, 18, 19);
		panel11.add(panel4p11);

		panel12 = new JPanelTitle();
		panel12.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel12.setBounds(182, 38, 108, 88);
		contentPane.add(panel12);
		panel12.setLayout(null);

		lblp12 = new JLabel("");
		lblp12.setHorizontalAlignment(SwingConstants.CENTER);
		lblp12.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp12.setBounds(10, 0, 88, 47);
		panel12.add(lblp12);

		panel1p12 = new JPanelTitle();
		panel1p12.setBounds(10, 58, 18, 19);
		panel12.add(panel1p12);

		panel2p12 = new JPanelTitle();
		panel2p12.setBounds(27, 58, 18, 19);
		panel12.add(panel2p12);

		panel3p12 = new JPanelTitle();
		panel3p12.setBounds(44, 58, 18, 19);
		panel12.add(panel3p12);

		panel5p12 = new JPanelTitle();
		panel5p12.setBounds(80, 58, 18, 19);
		panel12.add(panel5p12);

		panel4p12 = new JPanelTitle();
		panel4p12.setBounds(63, 58, 18, 19);
		panel12.add(panel4p12);

		panel13 = new JPanelTitle();
		panel13.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel13.setBounds(291, 38, 108, 88);
		contentPane.add(panel13);
		panel13.setLayout(null);

		lblp13 = new JLabel("");
		lblp13.setHorizontalAlignment(SwingConstants.CENTER);
		lblp13.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp13.setBounds(10, 0, 88, 47);
		panel13.add(lblp13);

		panel1p13 = new JPanelTitle();
		panel1p13.setBounds(10, 58, 18, 19);
		panel13.add(panel1p13);

		panel2p13 = new JPanelTitle();
		panel2p13.setBounds(27, 58, 18, 19);
		panel13.add(panel2p13);

		panel3p13 = new JPanelTitle();
		panel3p13.setBounds(44, 58, 18, 19);
		panel13.add(panel3p13);

		panel5p13 = new JPanelTitle();
		panel5p13.setBounds(80, 58, 18, 19);
		panel13.add(panel5p13);

		panel4p13 = new JPanelTitle();
		panel4p13.setBounds(63, 58, 18, 19);
		panel13.add(panel4p13);

		panel14 = new JPanelTitle();
		panel14.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel14.setBounds(400, 38, 108, 88);
		contentPane.add(panel14);
		panel14.setLayout(null);

		lblp14 = new JLabel("");
		lblp14.setHorizontalAlignment(SwingConstants.CENTER);
		lblp14.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp14.setBounds(10, 0, 88, 47);
		panel14.add(lblp14);

		panel1p14 = new JPanelTitle();
		panel1p14.setBounds(10, 58, 18, 19);
		panel14.add(panel1p14);

		panel2p14 = new JPanelTitle();
		panel2p14.setBounds(27, 58, 18, 19);
		panel14.add(panel2p14);

		panel3p14 = new JPanelTitle();
		panel3p14.setBounds(44, 58, 18, 19);
		panel14.add(panel3p14);

		panel5p14 = new JPanelTitle();
		panel5p14.setBounds(80, 58, 18, 19);
		panel14.add(panel5p14);

		panel4p14 = new JPanelTitle();
		panel4p14.setBounds(63, 58, 18, 19);
		panel14.add(panel4p14);

		panel15 = new JPanelTitle();
		panel15.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel15.setBounds(509, 38, 108, 88);
		contentPane.add(panel15);
		panel15.setLayout(null);

		lblp15 = new JLabel("");
		lblp15.setHorizontalAlignment(SwingConstants.CENTER);
		lblp15.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp15.setBounds(10, 0, 88, 47);
		panel15.add(lblp15);

		panel1p15 = new JPanelTitle();
		panel1p15.setBounds(10, 58, 18, 19);
		panel15.add(panel1p15);

		panel2p15 = new JPanelTitle();
		panel2p15.setBounds(27, 58, 18, 19);
		panel15.add(panel2p15);

		panel3p15 = new JPanelTitle();
		panel3p15.setBounds(44, 58, 18, 19);
		panel15.add(panel3p15);

		panel5p15 = new JPanelTitle();
		panel5p15.setBounds(80, 58, 18, 19);
		panel15.add(panel5p15);

		panel4p15 = new JPanelTitle();
		panel4p15.setBounds(63, 58, 18, 19);
		panel15.add(panel4p15);

		panel16 = new JPanelTitle();
		panel16.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel16.setBounds(618, 38, 108, 88);
		contentPane.add(panel16);
		panel16.setLayout(null);

		lblp16 = new JLabel("");
		lblp16.setHorizontalAlignment(SwingConstants.CENTER);
		lblp16.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp16.setBounds(10, 0, 88, 47);
		panel16.add(lblp16);

		panel1p16 = new JPanelTitle();
		panel1p16.setBounds(10, 58, 18, 19);
		panel16.add(panel1p16);

		panel2p16 = new JPanelTitle();
		panel2p16.setBounds(27, 58, 18, 19);
		panel16.add(panel2p16);

		panel3p16 = new JPanelTitle();
		panel3p16.setBounds(44, 58, 18, 19);
		panel16.add(panel3p16);

		panel5p16 = new JPanelTitle();
		panel5p16.setBounds(80, 58, 18, 19);
		panel16.add(panel5p16);

		panel4p16 = new JPanelTitle();
		panel4p16.setBounds(63, 58, 18, 19);
		panel16.add(panel4p16);

		panel10 = new JPanelTitle();
		panel10.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel10.setBounds(73, 127, 108, 88);
		contentPane.add(panel10);
		panel10.setLayout(null);

		lblp10 = new JLabel("");
		lblp10.setHorizontalAlignment(SwingConstants.CENTER);
		lblp10.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp10.setBounds(10, 0, 88, 47);
		panel10.add(lblp10);

		panel1p10 = new JPanelTitle();
		panel1p10.setBounds(10, 58, 18, 19);
		panel10.add(panel1p10);

		panel2p10 = new JPanelTitle();
		panel2p10.setBounds(27, 58, 18, 19);
		panel10.add(panel2p10);

		panel3p10 = new JPanelTitle();
		panel3p10.setBounds(44, 58, 18, 19);
		panel10.add(panel3p10);

		panel5p10 = new JPanelTitle();
		panel5p10.setBounds(80, 58, 18, 19);
		panel10.add(panel5p10);

		panel4p10 = new JPanelTitle();
		panel4p10.setBounds(63, 58, 18, 19);
		panel10.add(panel4p10);

		panel8 = new JPanelTitle();
		panel8.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel8.setBounds(73, 305, 108, 88);
		contentPane.add(panel8);
		panel8.setLayout(null);

		lblp8 = new JLabel("");
		lblp8.setHorizontalAlignment(SwingConstants.CENTER);
		lblp8.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp8.setBounds(10, 0, 88, 47);
		panel8.add(lblp8);

		panel1p8 = new JPanelTitle();
		panel1p8.setBounds(10, 58, 18, 19);
		panel8.add(panel1p8);

		panel2p8 = new JPanelTitle();
		panel2p8.setBounds(27, 58, 18, 19);
		panel8.add(panel2p8);

		panel3p8 = new JPanelTitle();
		panel3p8.setBounds(44, 58, 18, 19);
		panel8.add(panel3p8);

		panel5p8 = new JPanelTitle();
		panel5p8.setBounds(80, 58, 18, 19);
		panel8.add(panel5p8);

		panel4p8 = new JPanelTitle();
		panel4p8.setBounds(63, 58, 18, 19);
		panel8.add(panel4p8);

		panel9 = new JPanelTitle();
		panel9.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel9.setBounds(73, 216, 108, 88);
		contentPane.add(panel9);
		panel9.setLayout(null);

		lblp9 = new JLabel("");
		lblp9.setHorizontalAlignment(SwingConstants.CENTER);
		lblp9.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp9.setBounds(10, 0, 88, 47);
		panel9.add(lblp9);

		panel1p9 = new JPanelTitle();
		panel1p9.setBounds(10, 58, 18, 19);
		panel9.add(panel1p9);

		panel2p9 = new JPanelTitle();
		panel2p9.setBounds(27, 58, 18, 19);
		panel9.add(panel2p9);

		panel3p9 = new JPanelTitle();
		panel3p9.setBounds(44, 58, 18, 19);
		panel9.add(panel3p9);

		panel5p9 = new JPanelTitle();
		panel5p9.setBounds(80, 58, 18, 19);
		panel9.add(panel5p9);

		panel4p9 = new JPanelTitle();
		panel4p9.setBounds(63, 58, 18, 19);
		panel9.add(panel4p9);

		panel17 = new JPanelTitle();
		panel17.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel17.setBounds(618, 127, 108, 88);
		contentPane.add(panel17);
		panel17.setLayout(null);

		lblp17 = new JLabel("");
		lblp17.setHorizontalAlignment(SwingConstants.CENTER);
		lblp17.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp17.setBounds(10, 0, 88, 47);
		panel17.add(lblp17);

		panel1p17 = new JPanelTitle();
		panel1p17.setBounds(10, 58, 18, 19);
		panel17.add(panel1p17);

		panel2p17 = new JPanelTitle();
		panel2p17.setBounds(27, 58, 18, 19);
		panel17.add(panel2p17);

		panel3p17 = new JPanelTitle();
		panel3p17.setBounds(44, 58, 18, 19);
		panel17.add(panel3p17);

		panel5p17 = new JPanelTitle();
		panel5p17.setBounds(80, 58, 18, 19);
		panel17.add(panel5p17);

		panel4p17 = new JPanelTitle();
		panel4p17.setBounds(63, 58, 18, 19);
		panel17.add(panel4p17);

		panel18 = new JPanelTitle();
		panel18.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel18.setBounds(618, 216, 108, 88);
		contentPane.add(panel18);
		panel18.setLayout(null);

		lblp18 = new JLabel("");
		lblp18.setHorizontalAlignment(SwingConstants.CENTER);
		lblp18.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp18.setBounds(10, 0, 88, 47);
		panel18.add(lblp18);

		panel1p18 = new JPanelTitle();
		panel1p18.setBounds(10, 58, 18, 19);
		panel18.add(panel1p18);

		panel2p18 = new JPanelTitle();
		panel2p18.setBounds(27, 58, 18, 19);
		panel18.add(panel2p18);

		panel3p18 = new JPanelTitle();
		panel3p18.setBounds(44, 58, 18, 19);
		panel18.add(panel3p18);

		panel5p18 = new JPanelTitle();
		panel5p18.setBounds(80, 58, 18, 19);
		panel18.add(panel5p18);

		panel4p18 = new JPanelTitle();
		panel4p18.setBounds(63, 58, 18, 19);
		panel18.add(panel4p18);

		panel7 = new JPanelTitle();
		panel7.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel7.setBounds(73, 394, 108, 88);
		contentPane.add(panel7);
		panel7.setLayout(null);

		lblp7 = new JLabel("");
		lblp7.setHorizontalAlignment(SwingConstants.CENTER);
		lblp7.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp7.setBounds(10, 0, 88, 47);
		panel7.add(lblp7);

		panel1p7 = new JPanelTitle();
		panel1p7.setBounds(10, 58, 18, 19);
		panel7.add(panel1p7);

		panel2p7 = new JPanelTitle();
		panel2p7.setBounds(27, 58, 18, 19);
		panel7.add(panel2p7);

		panel3p7 = new JPanelTitle();
		panel3p7.setBounds(44, 58, 18, 19);
		panel7.add(panel3p7);

		panel5p7 = new JPanelTitle();
		panel5p7.setBounds(80, 58, 18, 19);
		panel7.add(panel5p7);

		panel4p7 = new JPanelTitle();
		panel4p7.setBounds(63, 58, 18, 19);
		panel7.add(panel4p7);

		panel20 = new JPanelTitle();
		panel20.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel20.setBounds(618, 394, 108, 88);
		contentPane.add(panel20);
		panel20.setLayout(null);

		lblp20 = new JLabel("");
		lblp20.setHorizontalAlignment(SwingConstants.CENTER);
		lblp20.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp20.setBounds(10, 0, 88, 47);
		panel20.add(lblp20);

		panel1p20 = new JPanelTitle();
		panel1p20.setBounds(10, 58, 18, 19);
		panel20.add(panel1p20);

		panel2p20 = new JPanelTitle();
		panel2p20.setBounds(27, 58, 18, 19);
		panel20.add(panel2p20);

		panel3p20 = new JPanelTitle();
		panel3p20.setBounds(44, 58, 18, 19);
		panel20.add(panel3p20);

		panel5p20 = new JPanelTitle();
		panel5p20.setBounds(80, 58, 18, 19);
		panel20.add(panel5p20);

		panel4p20 = new JPanelTitle();
		panel4p20.setBounds(63, 58, 18, 19);
		panel20.add(panel4p20);

		panel19 = new JPanelTitle();
		panel19.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel19.setBounds(618, 305, 108, 88);
		contentPane.add(panel19);
		panel19.setLayout(null);

		lblp19 = new JLabel("");
		lblp19.setHorizontalAlignment(SwingConstants.CENTER);
		lblp19.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp19.setBounds(10, 0, 88, 47);
		panel19.add(lblp19);

		panel1p19 = new JPanelTitle();
		panel1p19.setBounds(10, 58, 18, 19);
		panel19.add(panel1p19);

		panel2p19 = new JPanelTitle();
		panel2p19.setBounds(27, 58, 18, 19);
		panel19.add(panel2p19);

		panel3p19 = new JPanelTitle();
		panel3p19.setBounds(44, 58, 18, 19);
		panel19.add(panel3p19);

		panel5p19 = new JPanelTitle();
		panel5p19.setBounds(80, 58, 18, 19);
		panel19.add(panel5p19);

		panel4p19 = new JPanelTitle();
		panel4p19.setBounds(63, 58, 18, 19);
		panel19.add(panel4p19);

		panel1 = new JPanelTitle();
		panel1.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel1.setBounds(618, 483, 108, 88);
		contentPane.add(panel1);
		panel1.setLayout(null);

		lblp1 = new JLabel("");
		lblp1.setHorizontalAlignment(SwingConstants.CENTER);
		lblp1.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp1.setBounds(10, 0, 88, 47);
		panel1.add(lblp1);

		panel1p1 = new JPanelTitle();
		panel1p1.setBounds(10, 58, 18, 19);
		panel1.add(panel1p1);

		panel2p1 = new JPanelTitle();
		panel2p1.setBounds(27, 58, 18, 19);
		panel1.add(panel2p1);

		panel3p1 = new JPanelTitle();
		panel3p1.setBounds(44, 58, 18, 19);
		panel1.add(panel3p1);

		panel5p1 = new JPanelTitle();
		panel5p1.setBounds(80, 58, 18, 19);
		panel1.add(panel5p1);

		panel4p1 = new JPanelTitle();
		panel4p1.setBounds(63, 58, 18, 19);
		panel1.add(panel4p1);

		panel6 = new JPanelTitle();
		panel6.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel6.setBounds(73, 483, 108, 88);
		contentPane.add(panel6);
		panel6.setLayout(null);

		lblp6 = new JLabel("");
		lblp6.setHorizontalAlignment(SwingConstants.CENTER);
		lblp6.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp6.setBounds(10, 0, 88, 47);
		panel6.add(lblp6);

		panel1p6 = new JPanelTitle();
		panel1p6.setBounds(10, 58, 18, 19);
		panel6.add(panel1p6);

		panel2p6 = new JPanelTitle();
		panel2p6.setBounds(27, 58, 18, 19);
		panel6.add(panel2p6);

		panel3p6 = new JPanelTitle();
		panel3p6.setBounds(44, 58, 18, 19);
		panel6.add(panel3p6);

		panel5p6 = new JPanelTitle();
		panel5p6.setBounds(80, 58, 18, 19);
		panel6.add(panel5p6);

		panel4p6 = new JPanelTitle();
		panel4p6.setBounds(63, 58, 18, 19);
		panel6.add(panel4p6);

		panel5 = new JPanelTitle();
		panel5.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel5.setBounds(182, 483, 108, 88);
		contentPane.add(panel5);
		panel5.setLayout(null);

		lblp5 = new JLabel("");
		lblp5.setHorizontalAlignment(SwingConstants.CENTER);
		lblp5.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp5.setBounds(10, 0, 88, 47);
		panel5.add(lblp5);

		panel1p5 = new JPanelTitle();
		panel1p5.setBounds(10, 58, 18, 19);
		panel5.add(panel1p5);

		panel2p5 = new JPanelTitle();
		panel2p5.setBounds(27, 58, 18, 19);
		panel5.add(panel2p5);

		panel3p5 = new JPanelTitle();
		panel3p5.setBounds(44, 58, 18, 19);
		panel5.add(panel3p5);

		panel5p5 = new JPanelTitle();
		panel5p5.setBounds(80, 58, 18, 19);
		panel5.add(panel5p5);

		panel4p5 = new JPanelTitle();
		panel4p5.setBounds(63, 58, 18, 19);
		panel5.add(panel4p5);

		panel4 = new JPanelTitle();
		panel4.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel4.setBounds(291, 483, 108, 88);
		contentPane.add(panel4);
		panel4.setLayout(null);

		lblp4 = new JLabel("");
		lblp4.setHorizontalAlignment(SwingConstants.CENTER);
		lblp4.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp4.setBounds(10, 0, 88, 47);
		panel4.add(lblp4);

		panel1p4 = new JPanelTitle();
		panel1p4.setBounds(10, 58, 18, 19);
		panel4.add(panel1p4);

		panel2p4 = new JPanelTitle();
		panel2p4.setBounds(27, 58, 18, 19);
		panel4.add(panel2p4);

		panel3p4 = new JPanelTitle();
		panel3p4.setBounds(44, 58, 18, 19);
		panel4.add(panel3p4);

		panel5p4 = new JPanelTitle();
		panel5p4.setBounds(80, 58, 18, 19);
		panel4.add(panel5p4);

		panel4p4 = new JPanelTitle();
		panel4p4.setBounds(63, 58, 18, 19);
		panel4.add(panel4p4);

		panel3 = new JPanelTitle();
		panel3.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel3.setBounds(400, 483, 108, 88);
		contentPane.add(panel3);
		panel3.setLayout(null);

		lblp3 = new JLabel("");
		lblp3.setHorizontalAlignment(SwingConstants.CENTER);
		lblp3.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp3.setBounds(10, 0, 88, 47);
		panel3.add(lblp3);

		panel1p3 = new JPanelTitle();
		panel1p3.setBounds(10, 58, 18, 19);
		panel3.add(panel1p3);

		panel2p3 = new JPanelTitle();
		panel2p3.setBounds(27, 58, 18, 19);
		panel3.add(panel2p3);

		panel3p3 = new JPanelTitle();
		panel3p3.setBounds(44, 58, 18, 19);
		panel3.add(panel3p3);

		panel5p3 = new JPanelTitle();
		panel5p3.setBounds(80, 58, 18, 19);
		panel3.add(panel5p3);

		panel4p3 = new JPanelTitle();
		panel4p3.setBounds(63, 58, 18, 19);
		panel3.add(panel4p3);

		panel2 = new JPanelTitle();
		panel2.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel2.setBounds(509, 483, 108, 88);
		contentPane.add(panel2);
		panel2.setLayout(null);

		lblp2 = new JLabel("");
		lblp2.setHorizontalAlignment(SwingConstants.CENTER);
		lblp2.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblp2.setBounds(10, 0, 88, 47);
		panel2.add(lblp2);

		panel1p2 = new JPanelTitle();
		panel1p2.setBounds(10, 58, 18, 19);
		panel2.add(panel1p2);

		panel2p2 = new JPanelTitle();
		panel2p2.setBounds(27, 58, 18, 19);
		panel2.add(panel2p2);

		panel3p2 = new JPanelTitle();
		panel3p2.setBounds(44, 58, 18, 19);
		panel2.add(panel3p2);

		panel5p2 = new JPanelTitle();
		panel5p2.setBounds(80, 58, 18, 19);
		panel2.add(panel5p2);

		panel4p2 = new JPanelTitle();
		panel4p2.setBounds(63, 58, 18, 19);
		panel2.add(panel4p2);

		lblBackground = new JLabel("");
		lblBackground.setBounds(193, 148, 415, 184);
		ImageIcon icon = new ImageIcon(JStartGame.class.getResource("/imgs/monopoly.jpg"));
		Image img = icon.getImage();
		contentPane.add(lblBackground);
		lblBackground.setIcon(new ImageIcon(img.getScaledInstance(lblBackground.getWidth(), lblBackground.getHeight(), Image.SCALE_SMOOTH)));
		//		ImageIcon imgIcon = new ImageIcon("dado1.jpg");
		//		System.out.println(imgIcon);
		dados.add(new ImageIcon("dado1.jpg"));
		dados.add(new ImageIcon("dado2.jpg"));
		dados.add(new ImageIcon("dado3.jpg"));
		dados.add(new ImageIcon("dado4.jpg"));
		dados.add(new ImageIcon("dado5.jpg"));
		dados.add(new ImageIcon("dado6.jpg"));

		lblGif = new JLabel("");
		lblGif.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//				run(2000);
			}
		});
		lblGif.setBounds(367, 328, 59, 50);
		contentPane.add(lblGif);
	}
	public boolean repintarTablero() {
		JTablero.isReady = false;
		this.getElements();
		System.out.println("Paneles totales: " + panelArray.size());
		System.out.println("Labels totales: " + lblArray.size());
		Juego game = Executor.game;
		if (game != null) {
			if (!game.estaJuegoFinalizado()) {
				if (game.obtenerTablero() != null) {
					Integer h = 0;
					for (int i = 0 ; i < 20 ; i++) {
						System.out.println("CASILLA: " + game.obtenerTablero().getTablero().get(i).obtenerCategoria().toUpperCase());
						String msg = "";
						msg += "<html>" + game.obtenerTablero().getTablero().get(i).obtenerCategoria().toUpperCase() + "</html>";
						switch( game.obtenerTablero().getTablero().get(i).obtenerTipo().toUpperCase() )
						{
						case "SERVICIO PUBLICO":
							panelArray.get(h).setBackground(new Color(192, 247, 244));
							break;
						case "ESPECIAL":
							panelArray.get(h).setBackground(new Color(255, 186, 186));
							break;
						case "ALOJAMIENTO":
							panelArray.get(h).setBackground(new Color(186, 255, 186));
							break;
						case "SERVICIO PRIVADO":
							panelArray.get(h).setBackground(new Color(255, 255, 186));
							break;
						default:
							break;
						}
						h++;
			//OJO!
						lblArray.get(i).setText(msg);
						ArrayList<Jugador> jugs = game.obtenerTablero().getTablero().get(i).obtenerListaJugadoresEnCasilla();
						for (int v = 0 ; v < 5 ; v++) {
							panelArray.get(h).setBackground(Color.WHITE);
							h++;
						}
						h = h - 5;
						for (Jugador j : jugs) {
							panelArray.get(h).setBackground(JTablero.stringToColor(j.obtenerColor()));
							h++;
						}
						h += (5 - jugs.size()); 
					}
					JTablero.isReady = true;
					return true;

				} else {
					JOptionPane.showMessageDialog(null, "Error al acceder al tablero!");
					JTablero.isReady = true;
					return false;
				}
			} else {
				JOptionPane.showMessageDialog(null, "Juego Terminado!");
				JTablero.isReady = true;
				return false;
			}
		}
		JTablero.isReady = true;
		return false;
	}
	private void getElements() {
		lblArray = new ArrayList<JLabel>();
		panelArray = new ArrayList<JPanelTitle>();
		Component[] components = this.getContentPane().getComponents();
		Component[] comp1 = null;
		Object obj = null;
		
		//Bucle que utiliza reflexión para acceder al nombre de las variables y usarlo para establecerles el titulo.
		//También hace lo mismo con las JLabels pero con el nombre.
		for (Field field : JTablero.class.getDeclaredFields()) {
			try {
				obj = field.get(this);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (obj instanceof JPanelTitle) {
				((JPanelTitle)obj).setTitle(field.getName());
			} else if (obj instanceof JLabel) {
				((JLabel)obj).setName(field.getName());
			}
		}
		
		//Bucle que recoje todos los componentes y dependiendo de si són JPanelTitle o JLabel los añade a una array o a otra.
		for (int i=0; i < components.length; i++) {
			if (components[i] instanceof JLabel) {
				if (!((JLabel)components[i]).equals(JTablero.lblInstrucciones)) {
					lblArray.add((JLabel)components[i]);
				}
			} else if (components[i] instanceof JPanelTitle) {
				panelArray.add((JPanelTitle)components[i]);
				comp1 = ((JPanelTitle)components[i]).getComponents();
				for (int y=0; y < comp1.length; y++) {
					if (comp1[y] instanceof JLabel) {
						lblArray.add((JLabel)comp1[y]);
					} else if (comp1[y] instanceof JPanelTitle) {
						panelArray.add((JPanelTitle)comp1[y]);
					}
				}
			}
		}
		JTablero.ordenarComponentes();
	}
	
	private static Color stringToColor(String c) {
		Color color = null;
		switch (c.toUpperCase()) {
		case "AZUL":
			color = Color.BLUE;
			break;
		case "NEGRO":
			color = Color.BLACK;
			break;
		case "ROJO":
			color = Color.RED;
			break;
		case "AMARILLO":
			color = Color.YELLOW;
			break;
		case "VERDE":
			color = Color.GREEN;
			break;
		default:
			color = Color.WHITE;
			break;
		}
		return color;
	}
	public static void ejecutarDados(int milis, int pos) {
		JTablero.isReady = false;
		int times = 3;
		int waiting = milis / (dados.size()*times);
		
		//Bucle para crear una especie de GIF con los dados.
		for(int i = 0 ; i < times ; i++) {
			for (ImageIcon image : dados) {
				Image img = image.getImage();
				changeImage(img);
				lblGif.paint(lblGif.getGraphics());
				try {
					Thread.sleep(waiting);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		changeImage(dados.get(pos-1).getImage());
		JTablero.isReady = true;
	}

	public static void mostrarDado(int pos) {
		JTablero.isReady = false;
		changeImage(dados.get(pos-1).getImage());
		JTablero.isReady = true;
	}
	
	private static void changeImage(Image img) {
		lblGif.setIcon(new ImageIcon(img.getScaledInstance(lblGif.getWidth(), lblGif.getHeight(), Image.SCALE_SMOOTH)));
	}
	
	private static void ordenarComponentes() {
		ArrayList<JPanelTitle> bigPanels = new ArrayList<JPanelTitle>();
		ArrayList<JPanelTitle> littlePanels = new ArrayList<JPanelTitle>();
		Map<Integer, JPanelTitle> bigSortedPanels = new TreeMap<Integer, JPanelTitle>();
		Map<Integer, JPanelTitle> littleSortedPanels = new TreeMap<Integer, JPanelTitle>();
		Map<Integer, JLabel> sortedLabels = new TreeMap<Integer, JLabel>();
		String title = null, title2 = null, title3 = null;
		Integer numeroDelPanel = 0;
		
		//Recojer todos los JPanelTitle y separarlos en grandes y pequeños
		for (JPanelTitle jPanelTitle : panelArray) {
			title = jPanelTitle.getTitle();
			if (title.length() < 8) {
				bigPanels.add(jPanelTitle);
			} else {
				littlePanels.add(jPanelTitle);
			}
		}
		
		//Recojer todas las JLabel y ordenarlas con el numero como key en el mapa sortedLabels.
		for (JLabel lbl : lblArray) {
			if (!(lbl.equals(JTablero.lblInstrucciones) || lbl.equals(JTablero.lblBackground) || lbl.equals(JTablero.lblGif))) {
				title = lbl.getName().substring(4);
				System.out.println("Nombre: " + lbl.getName() + " Number: " + title);
				sortedLabels.put(Integer.parseInt(title), lbl);
			}
		}
		
		//Añadimos los paneles del array bigPanels al mapa bigSortedPanels ordenados con su numero.
		for (int i = 0 ; i < bigPanels.size() ; i++) {
			title = bigPanels.get(i).getTitle();
			numeroDelPanel = Integer.parseInt(title.substring(title.length()-((title.length() == 6) ? 1 : 2)));
			bigSortedPanels.put(numeroDelPanel, bigPanels.get(i));
		}

		//Añadimos los paneles del array littlePanels al mapa littleSortedPanels ordenados con su numero.
		//La key en este caso es el numero del panel pequeño (1) junto con el numero del panel grande donde se encuentra (10)
		//En este caso la key sera (110).
		for (int i = 0 ; i < littlePanels.size() ; i++) {
			title = littlePanels.get(i).getTitle();
			title2 = (title.substring(0, 6)).substring(5);
			title3 = title.substring(7, title.length());
			numeroDelPanel = Integer.parseInt(title2 + title3);
			littleSortedPanels.put(numeroDelPanel, littlePanels.get(i));
		}
		
		//Vaciamos los arrays generales de paneles y Labels para rellenarlos.
		panelArray.clear();
		lblArray.clear();
		
		//Bucle que va metiendo los paneles grandes y pequeños en orden (1 -> 5 ; 1 -> 5 ; ...).
		for (Integer i = 1 ; i <= 20 ; i++) {
			panelArray.add(bigSortedPanels.get(i));
			System.out.println("AÑADIDO: " + bigSortedPanels.get(i));
			bigSortedPanels.remove(i);
			lblArray.add(sortedLabels.get(i));
			sortedLabels.remove(i);
			for ( Integer h = 1 ; h <= 5 ; h++) {
				Integer num = Integer.parseInt(h.toString() + i.toString());
				panelArray.add(littleSortedPanels.get(num));
				System.out.println("\tAÑADIDO: " + littleSortedPanels.get(num));
				littleSortedPanels.remove(num);
			}
		}
		System.out.println("Paneles totales: " + panelArray.size());
		for (JPanelTitle jPanelTitle : panelArray) {
			System.out.println("PANEL: " + jPanelTitle.getTitle());
		}
	}
	
	public static void perderTiempo(String msg) {
		JTablero.isReady = false;
		JOptionPane panel = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{});
		JOptionPane.showMessageDialog(null, msg);
		JTablero.isReady = true;
	}
}
