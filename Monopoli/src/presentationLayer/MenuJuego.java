/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : MenuJuego.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que modela las distintas opciones / acciones del Menú Juego
 *
 ******************************************************************************/

package presentationLayer;

import applicationLayer.Juego;
import applicationLayer.Jugador;
import applicationLayer.casilla.ICasilla;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MenuJuego
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE CLASE                                                  |
    +------------------------------------------------------------------------*/
    private       int     opcionSeleccionada;
    private       int    errorExcepcion = 0;
    private       Scanner reader     = new Scanner( System.in )/*.useDelimiter( "\n" )*/;
    private       Juego   juego;
   
    /*------------------------------------------------------------------------+
    |   CONSTRUCTOR[ES] DE CLASE                                              |
    +------------------------------------------------------------------------*/

    /**
     * 
     * @param juego : instancia del juego actual.
     */
    public MenuJuego( Juego juego )
    {
        this.juego = juego;
    }

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICO[S]                                         |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     *
     * Muestra las opciones del Menú juego y recoge la opción elegida
     */
    public void mostrarYRecogerOpcionesMenu()
    {
        do
        {
            /* Aun no se ha producido ningún error / excepción */
            errorExcepcion = 0;

            /* Mostramos el estado de cada jugador */
            mostrarEstadoJugadores();

            /* Gestionamos la opción del Menú */
            gestionarOpcionMenu();

            /* Todo OK, continuamos... */
            switch( opcionSeleccionada )
            {
                case 1:
                    /* Tirar dado */
                    Utils.imprimirTitulo( "T I R A R   D A D O" );

                    if( juego.estaJuegoFinalizado() )
                    {
                        /* Juego terminado, no tiene sentido seguir jugando */
                        Utils.imprimirTextoFormateado( "La partida ya ha terminado y no es posible seguir jugando.", false );
                        Utils.imprimirTextoFormateado( "Vuelve al MENU PRINCIPAL e inicia una nueva partida.", false );
                        Utils.presionarIntroParaContinuar();
                    }
                    else
                    {
                        /* El juego aun continua. Gestionamos la tirada */
                        gestionarOpcionTirarDado();
                    }
                    break;
                case 2:
                    /* Ver tablero */
                    Utils.imprimirTitulo( "V E R   T A B L E R O" );
                    verTablero();
                    Utils.presionarIntroParaContinuar();
                    break;
                case 3:
                    /* Guardar Juego */
                    Utils.imprimirTitulo( "G U A R D A R   J U E G O" );
                    Utils.imprimirTextoFormateado( "¿Ruta donde guardar la partida?: ", true );

                    /* Capturamos el primer retorno de carro que no nos vale para nada */
                    String path = reader.nextLine();
                    /* Posteriormente alamacenamos la ruta introducida */
                    path = reader.nextLine();      
                    /* Guardamos la partida */
                    errorExcepcion = juego.guardar( path );

                    /* Comprobamos que no sa haya producido ningún error */
                    if( errorExcepcion != 0 )
                    {
                        /* Ha ocurrido un error en el proceso */
                        ErrorExcepcion.mostarInfo( errorExcepcion );
                    }
                    else
                    {
                        Utils.imprimirTextoFormateado( "Guardado correcto en " + path + "\\monopoly.sav", false );
                        Utils.presionarIntroParaContinuar();
                    }
                    break;
                case 4:
                    /* Salir del Menú */
                    break;
                default:
                    /* Opción errónea */
                    break;
            }
        }
        while( opcionSeleccionada != 4 );
    }

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PRIVADO[S]                                         |
    +------------------------------------------------------------------------*/
    
    /**-------------------------------------------------------------------------
     * 
     * Muestra por pantalla el estado de cada uno de los jugadores, incluyendo:
     * - Color.
     * - Nombre.
     * - Dinero.
     */
    private void mostrarEstadoJugadores()
    {
        /* Mostramos la cabecera del Menu */
        System.out.println();
        Utils.imprimeDecoracion( 1 );
        Utils.imprimeDecoracion( 2 );
        System.out.println("*         `·.¸¸.·´M E N U   D E L   J U E G O`·.¸¸.·´         *" );
        Utils.imprimeDecoracion( 2 );
        Utils.imprimeDecoracion( 1 );


        /* Mostramos el estado de cada uno de los jugadores */

        String separador = "    +-----------+-------------+-----------+--------+";

        System.out.println( "\n                +-------------+-----------+--------+" );
        System.out.println( "                |   NOMBRE    |   COLOR   | DINERO |" );
        System.out.println( separador );

        for( int i = 0; i < juego.obtenerListaJugadores().size(); i++ )
        {
            String nombre = juego.obtenerListaJugadores().get( i ).obtenerNombre();
            String color  = juego.obtenerListaJugadores().get( i ).obtenerColor();
            int    dinero = juego.obtenerListaJugadores().get( i ).obtenerDineroDisponible();

            /* Formateamos el texto */
            /*
               %b	Booleano
               %h	Hashcode
               %s	Cadena
               %c	Caracter unicode
               %d	Entero decimal
               %o	Entero octal
               %x	Entero hexadecimal
               %f	Real decimal
               %e	Real notación científica
               %g	Real notación científica o decimal
               %a	Real hexadecimal con mantisa y exponente
               %t	Fecha u hora

               El - es para justificar a la izquierda.
               El número es para que haga padding hasta completar el valor.

             */

            String info = String.format( "    | Jugador %s | %-11s | %-8s  | %-6d |", ( i + 1 ), nombre, color, dinero );

            /* Actualizamos la información */
            System.out.println( info );
            System.out.println( separador );
        }
    }
    
    /**-------------------------------------------------------------------------
     *
     * Tratamiento y recuperación de las opciones del Menu del juego.
     */
    private void gestionarOpcionMenu()
    {
        /* Mostramos las opciones del Menu principal */
        System.out.println();
        Utils.imprimeDecoracion( 1 );
        Utils.imprimeDecoracion( 2 );
        System.out.println( "*  1: Tirar dado.                                             *" );
        Utils.imprimeDecoracion( 2 );
        System.out.println( "*  2: Ver tablero.                                            *" );
        Utils.imprimeDecoracion( 2 );
        System.out.println( "*  3: Guardar juego.                                          *" );
        Utils.imprimeDecoracion( 2 );
        System.out.println( "*  4: Salir.                                                  *" );
        Utils.imprimeDecoracion( 2 );
        Utils.imprimeDecoracion( 1 );

        /** Control de errores y excepciones **********************************/
        do
        {
            /* Se pregrunta por el número de opción */
            System.out.println();
            Utils.imprimirTextoFormateado( "** Elige una opcion [1, 4]: ", true );
            
            /* Aun no se han producido errores de nivel */
            errorExcepcion = 0;

            try
            {
                /* Recuperamos la opción tecleada                    */
                /* Se comprueba que el valor este en el rango [1, 4] */
                opcionSeleccionada = reader.nextInt();
            }
            catch( NumberFormatException ex )
            {
                /* Entrada incorrecta */
                errorExcepcion = 9;
                /* Se informa de ello */
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }

            if( errorExcepcion == 0 && ( opcionSeleccionada < 1 ) || ( opcionSeleccionada > 4 ) )
            {
                /* Entrada incorrecta */
                errorExcepcion = 9;
                /* Se informa de ello */
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }
        }
        while( errorExcepcion != 0 );
        /** FIN Control *******************************************************/
    }   

    /**-------------------------------------------------------------------------
     * 
     * Gestión para la opción seleccionada de tirar el dado.
     */
    private void gestionarOpcionTirarDado()
    {
        /* Se informa de que jugador tira el dado */
        Utils.imprimirTextoFormateado( "Es el turno de " + juego.nombreJugadorConTurno() + ".", false );

        /* Se comprueba si el jugador está en la carcel */
        int casillaActual = juego.obtenerPosicionJugadorConTurnoEnTablero();

        /* Tira el dado                    */
        int resultadoDado = tirarDado();

        /* Se comprueba si origen es CARCEL */
        if( juego.obtenerCategoriaCasilla( casillaActual ).equals( ICasilla.CARCEL ) )
        {
            /* El juagador está en la carcer */
            Utils.imprimirTextoFormateado( "Actualmente estas en la CARCEL.", false );

            /* Se comprueba si se dispone de indulto */
            if( juego.estaJugadorConTurnoIndultado() )
            {
                /* Dispone de indulto */
                Utils.imprimirTextoFormateado( "Sin embargo puedes seguir jugando.", false );

                /* Se mueve a destino */
                moverJugadorConTurnoADestino( resultadoDado );
            }
            else
            {
                /* No dispone de indulto */

                /* Se gestiona el resultado del dado para la próxima tirada */
                if( resultadoDado == 5 )
                {
                    Utils.imprimirTextoFormateado( "Y consigues el indulto para la próxima tirada!!!", false );
                    juego.establecerIndultoAJugadorConTurno( true );
                }
                else
                {
                    Utils.imprimirTextoFormateado( "Y NO consigues el indulto para la próxima tirada!!", false );
                }

                /* Se prepara el próximo turno */
                prepararProximoTurno();
            }
        }
        else
        {
            /* El jugador no está en la carcel */

            /* Se mueve a destino */
            moverJugadorConTurnoADestino( resultadoDado );
        }
    }

    /**-------------------------------------------------------------------------
     *
     * @return el resultado sacado en la tirada.
     */
    private int tirarDado()
    {
        /* Se Tira el dado */
        int resultadoDado = juego.tirarDado();

        /* Se informa del valor obtenido */
        Utils.imprimirTextoFormateado( juego.nombreJugadorConTurno() + " ,tiras el dado y sacas un "  + resultadoDado, false );
        
        return resultadoDado;
    }  

    /**-------------------------------------------------------------------------
     * 
     * Mueve al jugador actual con turno a destino. Informando de cada paso.
     *
     * @param resultadoDado : Resultado obtenido en la tirada del dado.
     */
    private void moverJugadorConTurnoADestino( int resultadoDado )
    {
        /* Se mueve jugador a destino */
        int casillaActual = moverJugadorConTurnoYActualizarCasillas( resultadoDado );

        /* Se comprueba si destino es casilla ESPECIAL */
        if( juego.obtenerTipoCasilla( casillaActual ).equals( ICasilla.ESPECIAL ) )
        {
            /* El destino es una casilla ESPECIAL        */

            /* Se comprueba si además es casilla LOTERIA */
            if( juego.obtenerCategoriaCasilla( casillaActual ).equals( ICasilla.LOTERIA ) )
            {
                /* Se comprueba si jugador puede afrontar el pago */
                int pagoPorCaer = juego.obtenerPrecioPorCaerEnCasilla( casillaActual );

                /* Se informa de ello */
                Utils.imprimirTextoFormateado( "El pago por caer en esta casilla es de " + pagoPorCaer + " Euros.", false );

                if( juego.obtenerCantidadDineroDisponibleJugadorConTurno() < pagoPorCaer )
                {
                    /* El jugador no puede hacer frente al pago */
                    jugadorArruinado();
                } 
                else
                {
                    /* El jugador puede hacer frente al pago                 */
                    /* Se sustrae el pago al jugador de su dinero disponible */
                    juego.sustraerDineroAJugadorConTurno( pagoPorCaer );

                    /* Se informa al jugador que debe tirar el dado dos veces */                   
                    Utils.imprimirTextoFormateado( "Prueba suerte tirando el dado dos veces.", false );

                    BufferedReader teclado = new BufferedReader( new InputStreamReader( System.in ) );
                    int tirada1 = 0;
                    int tirada2 = 0;

                    /** Control de errores y excepciones **********************/
                    do
                    {
                        Utils.imprimirTextoFormateado( "Presiona INTRO para lanzar el dado en la primera tirada...", false );
                        
                        try
                        {
                            teclado.readLine();
                            tirada1 = tirarDado();
                        } 
                        catch( IOException ex )
                        {
                            errorExcepcion = 8;
                            ErrorExcepcion.mostarInfo( errorExcepcion );
                        }
                    }
                    while( errorExcepcion != 0 );

                    do
                    {
                        Utils.imprimirTextoFormateado( "Presiona INTRO para lanzar el dado en la segunda tirada...", false );
                        
                        try
                        {
                            teclado.readLine();
                            tirada2 = tirarDado();
                        } 
                        catch( IOException ex )
                        {
                            errorExcepcion = 8;
                            ErrorExcepcion.mostarInfo( errorExcepcion );
                        }
                    } 
                    while( errorExcepcion != 0 );
                    /** FIN Control *******************************************/

                    /* Se comprueba si el resultado es satisfactorio */
                    if( ( tirada1 == 6 ) && ( tirada2 == 6 ) )
                    {
                        /* Se aumenta un 10% el dinero del jugador agraciado */
                        juego.incrementarDineroEnPorcentajeAJugadorConTurno( 10 );

                        /* Se informa de ello */
                        Utils.imprimirTextoFormateado( "Enhorabuena!!! Te toco la LOTERIA", false );
                        Utils.imprimirTextoFormateado( "Incrementas tu dinero en un 10%.", false );

                        /* Se prepara el próximo turno */
                        prepararProximoTurno();
                    } 
                    else
                    {
                        /* El jugador no consigue el premio */
                        /* Se informa de ello */
                        Utils.imprimirTextoFormateado( "Lo siento no te toco la LOTERIA. Tal vez la proxima vez.", false );
                        
                        /* Se prepara el próximo turno      */
                        prepararProximoTurno();
                    }
                }
            } 
            else
            {
                /* El destino es la CARCEL, por lo tanto el jugador carece actualmente de indulto */
                juego.establecerIndultoAJugadorConTurno( false );
                
                /* Se prepara el próximo turno */                
                prepararProximoTurno();
            }
        } 
        else
        {
            /* El destino no es una casilla ESPECIAL */
            casillaNoEspecial( casillaActual );
        }
    }

    /**-------------------------------------------------------------------------
     *
     * Gestión de la parte del flujograma llamada CASILLA NO ESPECIAL
     *
     * @param casillaActual: Casilla actualmente en uso.
     */
    private void casillaNoEspecial( int casillaActual )
    {
        /* Casilla no especial */

        String nombreJugador = juego.nombreJugadorConTurno();

        /* Se comprueba si la casilla ha sido adquirida anteriormente */
        if( juego.estaCasillaAdquirida( casillaActual ) )
        {
            /* Casilla adquirida, no comprable */
            Jugador jugadorEnPosesion = juego.obtenerJugadorEnPosesionDeCasilla( casillaActual );

             /* Se informa de ello */
             Utils.imprimirTextoFormateado( "La casilla " + casillaActual + " esta adquirida por " + jugadorEnPosesion.obtenerNombre() + ".", false );

            /* Se comprueba si la casilla pertenece a otro jugador */
            if( nombreJugador.equals( jugadorEnPosesion.obtenerNombre() ) )
            {
                /* Casilla en posesión. Se apunta al próximo turno */
                prepararProximoTurno();
            }
            else
            {
                 /* Casilla perteneciente a otro jugador */

                 /* Se comprueba si el otro jugador está en la cárcel */
                 if( juego.obtenerCategoriaCasilla( jugadorEnPosesion.obtenerPosicionEnTablero() ).equals( ICasilla.CARCEL ) )
                 {
                     /* El otro jugador está en la cárcel, por lo tanto no es necesario el pago por caer */
                     Utils.imprimirTextoFormateado( "Propietario en la CARCEL, no es necesario ningun pago.", false );

                     /* Se apunta al próximo turno */
                     prepararProximoTurno();
                 }
                 else
                 {
                     /* El otro jugador NO está en la cárcel, por lo tanto es necesario el pago por caer */

                     /* Se comprueba el precio por el que ha de pagar por caer en la casilla */
                     int precioPorCaer = juego.obtenerPrecioPorCaerEnCasilla( casillaActual );

                     /* Se informa de ello */
                     Utils.imprimirTextoFormateado( "El precio a pagar por caer es de " + precioPorCaer + " Euros.", false );

                     /* Se comprueba si puede afrontar el pago */
                     if( juego.obtenerCantidadDineroDisponibleJugadorConTurno() < precioPorCaer )
                     {
                         /* El jugador no puede hacer frente al pago */
                         jugadorArruinado();
                     }
                     else
                     {
                         /* Se le sustrae el dinero del saldo disponible */
                         juego.sustraerDineroAJugadorConTurno( precioPorCaer );

                         /* Actualizamos el saldo */
                         Utils.imprimirTextoFormateado( "Valor sustraido de la cuenta de " + nombreJugador + ".", false );
                         Utils.imprimirTextoFormateado( "Saldo actualizado de " + juego.obtenerCantidadDineroDisponibleJugadorConTurno() + " Euros.", false );

                         /* Se le añade al saldo del jugador en posesión */
                         jugadorEnPosesion.aumentarDineroPorPago( precioPorCaer );

                         /* Actualizamos el saldo */
                         Utils.imprimirTextoFormateado( "Valor aumentado a la cuenta de " + jugadorEnPosesion.obtenerNombre() + ".", false );
                         Utils.imprimirTextoFormateado( "Saldo actualizado de " + jugadorEnPosesion.obtenerDineroDisponible() + " Euros.", false );

                         /* Se apunta al próximo turno */
                         prepararProximoTurno();
                     }
                 }
            }
        }
        else
        {
            /* Casilla no adquirida y por lo tanto comprable */
            String stAux;
            char   opcionElegidaDeCompra;
            int    precioDeCompra = juego.obtenerPrecioCompraCasilla( casillaActual );

            Utils.imprimirTextoFormateado( "Casilla en venta por un valor de " + precioDeCompra + " Euros.", false );

            /** Control de errores y excepciones ******************************/
            do
            {
                /* Aun no se ha producido ningún error / excepción */
                errorExcepcion = 0;

                Utils.imprimirTextoFormateado( "¿Desea comprarla? [Si(S)|No(N)]: ", true );

                stAux = reader.next().toLowerCase();

                opcionElegidaDeCompra = stAux.charAt( 0 );

                /* Se comprueba la respuesta */
                if( !( opcionElegidaDeCompra == 's' ) && !( opcionElegidaDeCompra == 'n' ) )
                {
                    /* Eleción de NO */
                    errorExcepcion = 7;
                    ErrorExcepcion.mostarInfo( errorExcepcion );
                }
            }
            while( errorExcepcion != 0 );
            /** FIN Control ***************************************************/

            /* Elección relaizada */
            if( opcionElegidaDeCompra == 's' )
            {
                /* El jugador quiere comprar la casilla         */

                /* Se comprueba si dispone de dinero suficiente */
                if( juego.obtenerCantidadDineroDisponibleJugadorConTurno() < precioDeCompra )
                {
                    /* Dinero insuficiente, Se informa de ello */
                    Utils.imprimirTextoFormateado( "Saldo insuficiente. No puedes realizar la compra!!!", false );

                    /* Apuntamos al próximo jugador */
                    prepararProximoTurno();
                }
                else
                {
                    /* Dinero suficiente, puede comprar la casilla */
                    
                    /* Se sustrae de la cuenta el valor de compra */
                    juego.sustraerDineroAJugadorConTurno( precioDeCompra );

                    /* Se añada la posesión al jugador */
                    juego.establecerCasillaEnPropiedadAJugadorConTurno( casillaActual );

                    /* Se informa de ello */
                    Utils.imprimirTextoFormateado( "Casilla adquirida. Se ha sustraido de tu cuenta " + precioDeCompra + " Euros.", false );
                    Utils.imprimirTextoFormateado( "Saldo actualizado de " + juego.obtenerCantidadDineroDisponibleJugadorConTurno() + " Euros.", false );

                    /* Apuntamos al próximo jugador */
                    prepararProximoTurno();
                }
            }
            else
            {
                /* El jugador no quier comprar la casilla */
                /* Apuntamos al próximo jugador           */
                prepararProximoTurno();
            }
        }
    }

    /**-------------------------------------------------------------------------
     *
     * Mueve al jugador con turno a destino. Actualizando las casillas origen y
     * destino.
     *
     * @param resultadoDado: Valor obtenido en la tirada con el dado.
     * @return la posición que ocupa la casilla ocupada.
     */
    private int moverJugadorConTurnoYActualizarCasillas( int resultadoDado )
    {
        /* Se mueve al jugador a la casilla que corresponda */
        int returnValue = juego.moverJugadorConTurnoACasilla( resultadoDado );

        /* Se informa de ello */        
        Utils.imprimirTextoFormateado( "Te mueves a la casilla " + returnValue + ".", false );

        /* Se comprueba de las propiedades de la casilla tipo y categoría */
        String tipoCasilla      = juego.obtenerTipoCasilla( returnValue );
        String categoriaCasilla = juego.obtenerCategoriaCasilla( returnValue );

        /* Se informa de ello */        
        Utils.imprimirTextoFormateado( "Que es del tipo " + tipoCasilla + ".", false);
        Utils.imprimirTextoFormateado( "Y tiene una categoria " + categoriaCasilla + ".", false);

        return returnValue;
    }

    /**-------------------------------------------------------------------------
     *
     *  Preparación para el próximo turno.
     */
    private void prepararProximoTurno()
    {
        /* Apuntamos al próximo jugador */
        juego.establecerTurnoSiguiente();

        /* Se informa de ello */
        Utils.imprimirTextoFormateado( "Fin de tu turno. Le toca al siguiente jugador.", false );
        /* Esperamos la aceptación */
        Utils.presionarIntroParaContinuar();
    }

    /**-------------------------------------------------------------------------
     * 
     * Gestión de acciones cuando un jugador no puede hacer frente a un pago.
     */
    private void jugadorArruinado()
    {
        /* Se informa de ello */
        Utils.imprimirTextoFormateado( "No puedes hacer frente al pago.", false );

        /* Se comprueba si al menos quedan 3 jugadores en la partida */
        if( juego.obtenerNumeroDeJugadores() > 3 )
        {
            /* Aun quedan varios jugadores... la partida continua           */
            /* Se devuelven las posesiones del jugador arruinado al tablero */
            juego.devolverAlJuegoPosesionesJugadorConTurno();

            Utils.imprimirTextoFormateado( "Se han devuelto tus posesiones al juego", false );

            /* Jugador Arruinado, se elimina del juego  */
            juego.eliminarJugadorConTurno();

            Utils.imprimirTextoFormateado( "Estas eliminado del juego. Los siento HAS PERDIDO!!!", false );

            /* Apuntamos al próximo jugador */
            prepararProximoTurno();
        }
        else
        {
            /* Sólo queda un jugador. El ganador */
            prepararProximoTurno();
            Utils.imprimirTextoFormateado( "Partida finalizada. Gana el jugador " + juego.nombreJugadorConTurno(), false );

            /* Finalizamos la partida            */
            juego.establecerJuegoFinalizado( true );

            /* Esperar validación para continuar */
            Utils.presionarIntroParaContinuar();
        }
    }

    /**------------------------------------------------------------------------
     * 
     * Muestra el tablero con formato CSV.
     * Cada fila corresponde a una casilla y cada columna codifica:
     * - Tipo de casilla.
     * - Precio de compra.
     * - Coste por caer.
     * - Adquirida por.
     * - Jugador[es] en la casilla.
     */
    private void verTablero()
    {        
        Utils.imprimeDecoracion( 3 );        

        for( int i = 1; i < juego.obtenerTamanoTablero(); i++ )
        {
            /* Por cada casilla de muestra... */
            
            /* Tipo de casilla */
            String stAux = juego.obtenerTablero().obtenerCategoriaCasillaEnPosicion( i ) + ";";

            /* Precio de compra */
            int aux = juego.obtenerTablero().obtenerPrecioCompraEnCasilla( i );

            if(  aux == -1 )
            {
                /* La casilla no es comprable, No se muestra nada */
                stAux += ";";
            }
            else
            {
                stAux += aux + ";";
            }

            /* Coste por caer */
            stAux += juego.obtenerTablero().obtenerCostePorCaerCasillaEnPosicion( i ) + ";";

            /* Adquirida por... */
            if( juego.obtenerTablero().obtenerJugadorEnPosesion( i ) == null )
            {
                /* Ningun jugador posee la casilla */
                stAux += ";";
            }
            else
            {
                stAux += juego.obtenerTablero().obtenerJugadorEnPosesion( i ).obtenerNombre() + ";";
            }            

            /* Juagador[es] en la casilla */   
            aux = juego.obtenerTablero().obtenerListaJugadoresEnCasilla( i ).size();

            if( aux == 0 )
            {
                /* No existen jugador[es] en la casilla */
                stAux += ";";
            }
            else
            {
                /* Existen jugador[es] en la casilla */
                for( int j = 0; j < aux; j++ )
                {
                    stAux += juego.obtenerTablero().obtenerListaJugadoresEnCasilla( i ).get( j ).obtenerNombre() + ";";
                }
            }

            /* Mostramo la info por consola */
            System.out.println( stAux );
        }

        //System.out.println();
    }
}
