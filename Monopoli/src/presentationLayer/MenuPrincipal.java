/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : MenuJuego.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase que modela las distintas opciones / acciones del Menú
 *             Principal. Además es la clase que contiene el método especial
 *             main.
 *
 ******************************************************************************/

package presentationLayer;

import applicationLayer.Juego;
import applicationLayer.Jugador;
import applicationLayer.Tablero;
import applicationLayer.casilla.*;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.ArrayList;
import java.util.Collections;

public class MenuPrincipal
{
    /*------------------------------------------------------------------------+
    |   VARIABLE[S] DE CLASE                                                  |
    +------------------------------------------------------------------------*/
    
    private static int     opcionSeleccionada;
    private static byte    errorExcepcion = 0;
    private static Scanner reader         = new Scanner( System.in )/*.useDelimiter( "\n" )*/;

    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PÚBLICO[S]                                         |
    +------------------------------------------------------------------------*/

    /**-------------------------------------------------------------------------
     * 
     * @param args: Argumentos de entrada desde consola
     */
    public static void main( String[] args )
    {           
        while( true )
        {
            /* Aun no se ha producido ningún error / excepción */
            errorExcepcion = 0;          

            /* Mostramos el Menú Principal */
            mostrarMenu();

            /* Recogemos la opción elegida */
            gestionarOpcionMenu();

            /* Todo OK, continuamos... */
            switch( opcionSeleccionada )
            {
                case 1:
                    /* Nueva partida */
                    Utils.imprimirTitulo( "N U E V A   P A R T I D A" );
                    crearNuevoJuego();
                    break;
                case 2:
                    /* Continuar partida */
                    Utils.imprimirTitulo( "C O N T I N U A R   P A R T I D A" );
                    continuarPartida();
                    break;
                case 3:
                    /* Creditos finales */
                    mostraCreditoFinal();
                    
                    /* Salir del juego */
                    System.exit( 0 );
                    break;
                default:
                    /* Opción errónea */
                    break;
            }
        }
    }    
   
    /*------------------------------------------------------------------------+
    |   MÉTODO[S] DE CLASE PRIVADO[S]                                         |
    +------------------------------------------------------------------------*/

    /**
     *
     */
    private static void mostraCreditoFinal()
    {
        System.out.println();
        System.out.println();
        System.out.println("     _____          ___   ");
        System.out.println("    /  /::\\        /__/|  ");
        System.out.println("   /  /:/\\:\\      |  |:|  ");
        System.out.println("  /  /:/~/::\\     |  |:|  ");
        System.out.println(" /__/:/ /:/\\:|  __|__|:|  ");
        System.out.println(" \\  \\:\\/:/~/:/ /__/::::\\  ");
        System.out.println("  \\  \\::/ /:/     ~\\~~\\:\\ ");
        System.out.println("   \\  \\:\\/:/        \\  \\:\\");
        System.out.println("    \\  \\::/          \\__\\/");
        System.out.println("     \\__\\/                ");
        System.out.println("                    ___           ___           ___                 ");
        System.out.println("                   /  /\\         /__/\\         /  /\\        ___     ");
        System.out.println("                  /  /::\\        \\  \\:\\       /  /:/_      /  /\\    ");
        System.out.println("  ___     ___    /  /:/\\:\\        \\  \\:\\     /  /:/ /\\    /  /:/    ");
        System.out.println(" /__/\\   /  /\\  /  /:/  \\:\\   _____\\__\\:\\   /  /:/_/::\\  /__/::\\    ");
        System.out.println(" \\  \\:\\ /  /:/ /__/:/ \\__\\:\\ /__/::::::::\\ /__/:/__\\/\\:\\ \\__\\/\\:\\__ ");
        System.out.println("  \\  \\:\\  /:/  \\  \\:\\ /  /:/ \\  \\:\\~~\\~~\\/ \\  \\:\\ /~~/:/    \\  \\:\\/\\");
        System.out.println("   \\  \\:\\/:/    \\  \\:\\  /:/   \\  \\:\\  ~~~   \\  \\:\\  /:/      \\__\\::/");
        System.out.println("    \\  \\::/      \\  \\:\\/:/     \\  \\:\\        \\  \\:\\/:/       /__/:/ ");
        System.out.println("     \\__\\/        \\  \\::/       \\  \\:\\        \\  \\::/        \\__\\/  ");
        System.out.println("                   \\__\\/         \\__\\/         \\__\\/                ");
        System.out.println();
        System.out.println();
    }

    /**-------------------------------------------------------------------------
     * 
     * Visualización de las opciones del Menú principal
     */
    private static void mostrarMenu()
    {
        /* Mostramos la cabecera del Menu */    
        System.out.println();        
        Utils.imprimeDecoracion( 1 );
        Utils.imprimeDecoracion( 2 );
        System.out.println("*         `·.¸¸.·´M E N U   P R I N C I P A L`·.¸¸.·´         *" );
        Utils.imprimeDecoracion( 2 );       
        Utils.imprimeDecoracion( 1 );

        /* Mostramos las opciones del Menu principal */       
        Utils.imprimeDecoracion( 2 );
        System.out.println( "*  1: Nueva Partida.                                          *" );        
        Utils.imprimeDecoracion( 2 );
        System.out.println( "*  2: Continuar partida.                                      *" );       
        Utils.imprimeDecoracion( 2 );
        System.out.println( "*  3: Salir.                                                  *" );     
        Utils.imprimeDecoracion( 2 );        
        Utils.imprimeDecoracion( 1 );
    }

    /**-------------------------------------------------------------------------
     *
     * Gestión de las distintas opciones del menu
     */
    private static void gestionarOpcionMenu()
    {
        do
        {
            /* Aun no se ha producido un error de nivel */
            errorExcepcion = 0;

            /* Se pregrunta por el número de opción */
            System.out.println();
            Utils.imprimirTextoFormateado( "** Elige una opcion [1, 3]: ", true );

            /** Control de errores y excepciones ******************************/
            try
            {
                /* Recuperamos la opción tecleada                    */
                /* Se comprueba que el valor este en el rango [1, 3] */
                opcionSeleccionada = Integer.valueOf( reader.next() );            
            } 
            catch( NumberFormatException ex )
            {
                /* Entrada incorrecta */
                errorExcepcion = 1;
                /* Se informa de ello */
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }

            if( errorExcepcion == 0 && ( opcionSeleccionada < 1 ) || ( opcionSeleccionada > 3 ) )
            {
                /* Entrada incorrecta */
                errorExcepcion = 1;
                /* Se informa de ello */
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }
            /** FIN Control ***************************************************/
        }
        while( errorExcepcion != 0 );
    }

    /**-------------------------------------------------------------------------
     * 
     * Creación de una nueva partida
     */
    private static void crearNuevoJuego()
    {
        int                  nJugadores     = 0;
        int                  tamanoTablero  = 0;
        ArrayList< Jugador > listaJugadores = new ArrayList< Jugador >();
        Tablero              tablero;

        /* 1º: Solicitud del número de jugadores */
        nJugadores = obtenerNumeroJugadores();

        /* 1.a: Solicitud del nombre del jugador */
        obtenerNombresJugadores( nJugadores, listaJugadores );

         /* 1.b: Solicitud del color */
        obtenerColoresJugadores( nJugadores, listaJugadores );

        /* 2º: Solicitud del tamaño del tablero */
        tamanoTablero = obtenerTamanoTablero();

        /* 3º: Generación del tablero */
        tablero = generarTablero( tamanoTablero );
        /* Se genera un nuevo Juego */
        Juego juego = new Juego( listaJugadores, tablero );

        /* 4º: Ir al menú juego */
        MenuJuego menuJuego = new MenuJuego( juego );
        menuJuego.mostrarYRecogerOpcionesMenu();
    }

    /**-------------------------------------------------------------------------
     *
     * Diálogo y gestión para continuar una partida.
     */
    private static void continuarPartida()
    {
        Utils.imprimirTextoFormateado( "¿Ruta donde cargar la partida?: ", true );

        /* Capturamos el primer retorno de carro que no nos vale para nada */
        String path = reader.nextLine();
        
        /* Posteriormente alamacenamos la ruta introducida */
        path = reader.nextLine();

        /* Recuperamos la partida guardada */
        Juego juego =  new Juego( null, null ).continuar( path );

        if( juego == null )
        {
            errorExcepcion = 11;
            /* Ha ocurrido un error en el proceso */
            ErrorExcepcion.mostarInfo( errorExcepcion );
        }
        else
        {
            /* Todo Ok, informamos de ello */
            Utils.imprimirTextoFormateado( "Partida cargada correctamente.", false );
            Utils.presionarIntroParaContinuar();
            
            /* Vamos al menu juego */
            MenuJuego menuJuego = new MenuJuego( juego );
            menuJuego.mostrarYRecogerOpcionesMenu();
        }
    }

    /**-------------------------------------------------------------------------
     *
     * Diálogo y gestión del número de jugadores.
     * 
     * @return El número de jugadores que comienzan un juego.
     */
    private static int obtenerNumeroJugadores()
    {
        /* En primera instancia no existen jugadores */
        int nJugadores = 0;
         
        do
        {
            /* Aun no se ha producido ningún error / excepción */
            errorExcepcion = 0;
            
            /* Petición del número de usuarios */
            Utils.imprimirTextoFormateado( "Numero de jugadores [2, 5]: ", true );

            try
            {
                /* Recuperamos la opción tecleada                    */
                /* Se comprueba que el valor este en el rango [1, 5] */
                nJugadores = Integer.valueOf( reader.next() );
                /* Evitmos efectos indeseados */
                reader.nextLine();
            } 
            catch( NumberFormatException ex )
            {
                errorExcepcion = 2;
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }

            if( ( errorExcepcion == 0 ) && ( ( nJugadores < 2 ) || ( nJugadores > 5 ) ) )
            {
                errorExcepcion = 2;
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }
        } 
        while( errorExcepcion != 0 );
        
        return nJugadores;
    }

   /**--------------------------------------------------------------------------
    *
    * Dialogo y gestión de los nombres de los jugadores.
    *
    * @param nJugadores    : Número de jugadores.
    * @param listaJugadores: lista donde se alamacenarán los jugadores.
    */
    private static void obtenerNombresJugadores( int nJugadores, ArrayList< Jugador > listaJugadores )
    {
        ArrayList< String >  listaNombres   = new ArrayList< String >();

        for( int i = 0; i < nJugadores; i++ )
        {
            /* 1.a: Solicitud del nombre del jugador */
            do
            {
                /* Aun no se ha producido ningún error / excepción */
                errorExcepcion = 0;

                Utils.imprimirTextoFormateado( "Nombre jugador " + ( i + 1 ) + " [max. 10 caracteres]: ", true );

                String auxSt = reader.nextLine();

                if( listaNombres.contains( auxSt )  )
                {
                    /* El nombre ya está en uso */
                    errorExcepcion = 4;
                    ErrorExcepcion.mostarInfo( errorExcepcion );
                }
                else if( auxSt.length() > 10 )
                {
                    /* Longitud de nombre excesiva */
                    errorExcepcion = 5;
                    ErrorExcepcion.mostarInfo( errorExcepcion );
                }
                else
                {
                    listaNombres.add( auxSt );                   
                }
            } 
            while( errorExcepcion != 0 );
        }

        /* Ordenamos los nombres alfabeticamente */
        Collections.sort( listaNombres );

        for( byte i = 0; i < nJugadores; i++ )
        {
            /* Creamos un objeto jugador con su nombre, la posición de salida y 10000 euros */
            Jugador auxJugador = new Jugador( listaNombres.get( i ), ( byte )0, 10000 );
            /* Lo alamcenamos el la lista de jugadores */
            listaJugadores.add( auxJugador );
        }
    }

    /**-------------------------------------------------------------------------
     *
     * Dialogo y gestión del color de los jugadores.
     *
     * @param nJugadores     : Número de jugadores.
     * @param listaJugadores : Lista de jugadores.
     */
    private static void obtenerColoresJugadores( int nJugadores, ArrayList< Jugador > listaJugadores )
    {
        ArrayList< String >  listaColores   = new ArrayList< String >();

        /* Inicializamos la lista de colores */
        listaColores.add( "azul" );
        listaColores.add( "negro" );
        listaColores.add( "rojo" );
        listaColores.add( "amarillo" );
        listaColores.add( "verde" );
        
        for( int i = 0; i < nJugadores; i++ )
        {           
            do
            {
                /* Aun no se ha producido ningún error / excepción */
                errorExcepcion = 0;

                String auxSt = "Color jugador " + ( i + 1 ) + " [";

                for( int j = 0; j < listaColores.size(); j++ )
                {
                    auxSt = auxSt + listaColores.get( j );
                    if( j != ( listaColores.size() - 1 ) )
                    {
                        auxSt = auxSt + ", ";
                    }
                }
                auxSt = auxSt + "]: ";

                /* Formulamos la petición */
                Utils.imprimirTextoFormateado( auxSt, true );
                /* Recogemos el color elegido */
                auxSt = reader.next().toLowerCase();
                reader.nextLine();

                if( listaColores.contains( auxSt ) )
                {
                    /* Elección de color correcta */
                    listaJugadores.get( i ).establecerColor( auxSt );
                    listaColores.remove( auxSt );
                } 
                else
                {
                    /* Elección de color incorrecta */
                    errorExcepcion = 6;
                    ErrorExcepcion.mostarInfo( errorExcepcion );
                }
            } 
            while( errorExcepcion != 0 );
        }
    }

    /**-------------------------------------------------------------------------
     *
     * Dialogo y gestión del tamaño del tablero.
     * 
     * @return el tamaño del tablero.
     */
    private static int obtenerTamanoTablero()
    {
        int tamanoTablero = 0;

        do
        {
            /* Aun no se ha producido ningún error / excepción */
            errorExcepcion = 0;

            /* Petición del tamaño de tablero */
            Utils.imprimirTextoFormateado( "Tamano del tablero [20, 100]: ", true );
            
            try
            {
                /* Recuperamos la opción tecleada                       */
                /* Se comprueba que el valor este en el rango [20, 100] */
                tamanoTablero = Integer.valueOf( reader.next() );
                //reader.nextLine();
            } 
            catch( InputMismatchException ex )
            {
                errorExcepcion = 3;
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }

            if( errorExcepcion == 0 && ( ( tamanoTablero < 20 ) || ( tamanoTablero > 100 ) ) )
            {
                errorExcepcion = 3;
                ErrorExcepcion.mostarInfo( errorExcepcion );
            }
            else
            {
                /* Comprobamos que el valor sea divisible entre 10 */
                if( ( tamanoTablero % 10 ) != 0 )
                {
                    errorExcepcion = 3;
                    ErrorExcepcion.mostarInfo( errorExcepcion );
                }
            }
        } 
        while( errorExcepcion != 0 );

        return tamanoTablero;
    }

   /**--------------------------------------------------------------------------
    *
    * Dialogo y gestión de un tablero de juego.
    *
    * @param tamanoTablero: Tamaño del tablero
    * @return             : El tablero generado.
    */
    private static Tablero generarTablero( int tamanoTablero )
    {
        ArrayList< Casilla > tablero           = new ArrayList< Casilla >();
        byte                 nCasillasLoteria  = ( byte )( tamanoTablero / 10 );
        byte                 nCasillasCarcel   = ( byte )( tamanoTablero / 10 );
        byte                 casillasrestantes = ( byte )( tamanoTablero - ( nCasillasCarcel + nCasillasLoteria ) );

        /* Añadimos las casillas cárceles */
        for( byte i = 0; i < nCasillasCarcel; i++ )
        {
            tablero.add( new CasillaCarcel() );
        }

        /* Añadimos las casillas loterías al tablero */
        for( byte i = 0; i < nCasillasLoteria; i++ )
        {
            tablero.add( new CasillaLoteria() );
        }

        /* Añadimos el resto de casillas de manera equitativa + ó - */
        for( byte i = 0; i < casillasrestantes; i++ )
        {            
            switch( i % 9 )
            {
                case 0:
                    tablero.add( new CasillaUnifamiliar() );
                    break;
                case 1:
                    tablero.add( new CasillaEdificio() );
                    break;
                case 2:
                    tablero.add( new CasillaCompaniaAgua() );
                    break;
                case 3:
                    tablero.add( new CasillaCompaniaGas() );
                    break;
                case 4:
                    tablero.add( new CasillaCompaniaLuz() );
                    break;
                case 5:
                    tablero.add( new CasillaCompaniaTelefono() );
                    break;
                case 6:
                    tablero.add( new CasillaCentroComercial() );
                    break;
                case 7:
                    tablero.add( new CasillaHotel() );
                    break;
                case 8:
                    tablero.add( new CasillaRestaurante() );
                    break;
                default:
                    break;
            }
        }

        /* Se mezclan las casillas aleatoriamente un número aleatorio de veces */
        for(  byte i = 0; i < ( ( System.currentTimeMillis() % 9 ) + 1 ); i++ )
        {
            Collections.shuffle( tablero );
        }

        /* Añadimos al principio de la lista la casilla 0 que será la de salida */
        tablero.add( 0, new CasillaSalida() );

        /* Asignamos a cada casilla la posición que le corresponde en el tablero */
        for( byte i = 0; i < tablero.size(); i++ )
        {
            tablero.get( i ).establecerPosicionEnTablero( ( byte )( i ) );
        }

        /* Retornamos el tablero */
        return( new Tablero( tablero ) );
    }   
}




