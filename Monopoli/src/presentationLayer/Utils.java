/*******************************************************************************
 *
 * AUTOR     : Longinos Recuero Bustos
 * FECHA     : 19/03/2011
 * EMAIL     : lrecuero1@alumno.uned.es
 * BLOG      : http:\\longinox.blogspot.com
 * LICENCIA  : GPL
 * NOMBRE    : Utils.java
 * VERSION   : 1.0
 * DEFINICIÓN: Clase utilitaria que ayuda a formatear la salida por consola.
 *
 ******************************************************************************/

package presentationLayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils
{
     private final static String  decor_1 = "***************************************************************";
     private final static String  decor_2 = "*                                                             *";
     private final static String  decor_3 = "+-------------------------------------------------------------+";

    /**-------------------------------------------------------------------------
     * 
     * Método que nos permite controla cuando un string es nulo o vacío
     *
     * @param s : El string a gestionar.
     * @return true si el "texto" es nulo o vacio o false en otro caso.
     */
    public static boolean esNullOVacio( String texto )
    {
        return( texto == null || texto.trim().equals( "" ) );
    }

    /**-------------------------------------------------------------------------
     *
     * Método que nos permite imprimir un "texto" de manera decorada.
     *
     * @param texto      : El string a gestionar.
     * @param sinRetorno : true si se quiere un tetorno al final o false en caso
     *                     contrario.
     */
    public static void imprimirTextoFormateado( String texto, boolean sinRetorno )
    {
        imprimeDecoracion( 3 );

        if( sinRetorno )
        {
            System.out.print( "| " + texto );
        }
        else
        {
            System.out.println( "| " + texto );
        }
        
    }   

    /**-------------------------------------------------------------------------
     *
     * Método que imprime un título "texto" de manera decorada.
     * 
     * @param texto : El string a gestionar.
     */
    public static void imprimirTitulo( String texto )
    {
        byte   i;
        String st = " ";

        imprimeDecoracion( 3 );

        for( i = 1; i < ( ( 61 - texto.length() ) / 2 ); i++ )
        {
            st += " ";
        }

        st += texto;
        
        System.out.println( String.format( "|%-61s|", st ) );

    }

    /**-------------------------------------------------------------------------
     * 
     */
    public static void presionarIntroParaContinuar()
    {
        byte errorExcepcion;

        BufferedReader teclado = new BufferedReader( new InputStreamReader( System.in ) );

        imprimirTextoFormateado( "Presiona INTRO para continuar...", false );

        try
        {
            teclado.readLine();
        }
        catch( IOException ex )
        {
             errorExcepcion = 8;
             ErrorExcepcion.mostarInfo( errorExcepcion );
        }        
    }

    /**-------------------------------------------------------------------------
     *
     */
    public static void imprimeDecoracion( int decoracion )
    {
        switch( decoracion )
        {
            case 1:
                System.out.println( decor_1 );
                break;
            case 2:
                System.out.println( decor_2 );
                break;
            case 3:
                System.out.println( decor_3 );
                break;
            default:
                break;
        }
    }
}

